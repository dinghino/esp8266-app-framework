#include "config.strip.h"
// #include <StripController.h>
#include <FastLED.h>
#include <lightFunctions.h>
#include <eaf/utils/logger.h>


void payloadToCharArr(byte* payload, uint8_t length, char* buffer)
{
    // char buffer[length];
    for (int i = 0; i < length; i++)
        buffer[i] = payload[i];
    buffer[length] = '\0';
    // return buffer;
}
uint payloadToInt(byte *payload, uint8_t length)
{
    char buffer[length];  payloadToCharArr(payload, length, buffer);
    unsigned int value = atoi(buffer);
    return value;
}

void stripTest(std::vector<CRGB*> strips)
{
    utils::Log("FLED", "Testing strip");
    static const uint speed = 7;

    FastLED.clear();
    LEDS.setBrightness(84);

    for (int l = 0; l < 2; l++) {
        static uint8_t hue = 0;
        // First slide the led in one direction
        for (int i = 0; i < LEDS_NUM; i++)
        {
            for (auto strip : strips)
                strip[i] = CHSV(hue++, 255, 255);

            FastLED.show(); 
            fadeAll(strips);
            delay(speed);
        }
        
        // Now go in the other direction.  
        for (int i = (LEDS_NUM)-1; i >= 0; i--)
        {
            for (auto strip : strips)
                strip[i] = CHSV(hue++, 255, 255);

            FastLED.show();
            fadeAll(strips);
            delay(speed);
        }
    }
    FastLED.clear();
    FastLED.show();
}

void fadeAll(std::vector<CRGB*> strips)
{
    for(int i = 0; i < LEDS_NUM; i++)
    {
        for (auto strip : strips)
            strip[i].nscale8(250);
    }
}


void setColor(std::vector<CRGB*> strips, uint color, int delay_)
{
    for (int i = 0; i < LEDS_NUM; i++)
    {
        for (auto strip : strips)
            strip[i] = color;
        FastLED.show();
        delay(delay_);
    }
    FastLED.show();
}

