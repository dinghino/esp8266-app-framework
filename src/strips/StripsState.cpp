#include <ArduinoJson.h>

#include "strips/Color.h"
#include "strips/StripsState.h"
#include "config.global.h"

eaf::LedStrips::StripsState::StripsState()
{
}

void eaf::LedStrips::StripsState::addLeds(CRGB* led)
{
    leds.push_back(led);
}

const char* eaf::LedStrips::StripsState::serialize() const
{
    char buffer[CMD_STATE_SIZE];
    DynamicJsonDocument doc(CMD_STATE_SIZE);

    doc["brightness"] = brightness;
    doc["color_temp"] = color_temp;

    JsonObject col = doc.createNestedObject("color");
    col["r"] = color.rgb.r;
    col["g"] = color.rgb.g;
    col["b"] = color.rgb.b;

    doc["effect"] = effect.c_str();
    doc["state"] = state.c_str();
    doc["transition"] = transition;
    doc["white_value"] = white_value;

    serializeJson(doc, buffer);
    return buffer;
}

