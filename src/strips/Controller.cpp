#include <eaf/core/App.h>
#include "strips/Color.h"
#include "strips/Controller.h"
#include "strips/StripEvent.h"

eaf::LedStrips::Controller::Controller() {}


void eaf::LedStrips::Controller::addLeds(CRGB* strip)
{
    strips.leds.push_back(strip);
}
std::vector<CRGB*>& eaf::LedStrips::Controller::getLeds()
{
    return strips.leds;
}

void eaf::LedStrips::Controller::begin()
{}
void eaf::LedStrips::Controller::update()
{
    if (changes.hasChanges())
        updateLights();
}

// Primary entry point for handling new state
void eaf::LedStrips::Controller::handleNewState(char* newState)
{
    DynamicJsonDocument state(CMD_STATE_SIZE);
    deserializeJson(state, newState);

    if (state.containsKey("state"))
    {
        setState(state["state"].as<const char*>());
    }

    if (state.containsKey("color"))
    {
        auto col = state["color"];
        if (col.containsKey("r"))
            setColor(RGB(col["r"], col["g"], col["b"]));
    }

    if (state.containsKey("brightness"))
    {
        setBrightness(state["brightness"].as<uint>());
    }
    // updateLights();
}
 
 // automatic updaters based on internal state
void eaf::LedStrips::Controller::updateState()
{
    for (auto leds : strips.leds)
        toggle(leds);
}

void eaf::LedStrips::Controller::updateBrightness()
{
    updateBrightness(strips.brightness);
}

void eaf::LedStrips::Controller::updateColor()
{
    for (auto leds : strips.leds)
        updateColor(leds);
}

// Controller internals ======================================================
// Calls the various update fuunctions if changes occured
void eaf::LedStrips::Controller::updateLights()
{
    if (changes.color)
        updateColor();

    if (changes.brightness)
        updateBrightness();

    if (changes.state)
        updateState();

    changes.reset();
    app().emit(StripEvent(strips));
}
// sub updaters
// === COLOR ------------------------------------------------------------------
void eaf::LedStrips::Controller::updateColor(CRGB* strip)
{
    if (strips.color.rgb == RGB{ 0, 0, 0 })
        strips.color.rgb = RGB{ 255, 255, 255 };

    updateColor(strip, strips.color.rgb);
}
void eaf::LedStrips::Controller::updateColor(CRGB* strip, RGB color)
{
    logUpdate("COLOR", "%3d, %3d, %3d", color.r, color.g, color.b);
    for (int i = 0; i < LEDS_NUM; i++)
    {
        strip[i].r = color.r;
        strip[i].g = color.g;
        strip[i].b = color.b;
        FastLED.show();
        delay(m_rgb_step_time);
    }
}

// === BRIGHTNESS -------------------------------------------------------------

void eaf::LedStrips::Controller::updateBrightness(uint8_t target)
{
    logUpdate("BRIGHTNESS", "%d", target);
    uint8_t current = FastLED.getBrightness();

    if (target == current)
        return;

    int direction = current < target ? 1 : -1;

    while (FastLED.getBrightness() != target)
    {
        FastLED.setBrightness(FastLED.getBrightness() + (m_brgight_step_size * direction));
        FastLED.show();
        delay(m_brgight_step_time);
    }
    FastLED.show();
}

// === STATE ------------------------------------------------------------------
void eaf::LedStrips::Controller::toggle(CRGB* strip)
{
    logUpdate("STATUS", "%s", strips.state.c_str());
    if (m_status == false)
        turnOff(strip);
    else
        turnOn(strip);
}
void eaf::LedStrips::Controller::turnOn(CRGB* strip)
{
    updateColor(strip);
    updateBrightness();
}
void eaf::LedStrips::Controller::turnOff(CRGB* strip)
{
    updateColor(strip, RGB{ 0, 0, 0 });
}

void eaf::LedStrips::Controller::setInternalState(const char* newState)
{
    m_status = strcmp(newState, "ON") == 0 ? true : false;
}

// template <typename ...Args>
void eaf::LedStrips::Controller::logUpdate(const char* prop, const char* format, ...)
{
    if (!EAF_LEDS_LOG_UPDATES)
        return;

    char buffer[256];
    va_list args;
    va_start (args, format);
    vsnprintf (buffer, 255, format, args);

    Serial.printf("UPDATING %-20s -> %s\n", prop, buffer);
    va_end (args);
}
