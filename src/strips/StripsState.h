#ifndef __EAF_LED_STRIPS__STRIPS_STATE_H__
#define __EAF_LED_STRIPS__STRIPS_STATE_H__

#include <string>
#include <vector>
#include <FastLED.h>

namespace eaf { namespace LedStrips {

class StripsState
{
public:
    Color       color;
    uint        brightness;
    uint        color_temp;
    uint        transition;
    uint        white_value;
    std::string effect;
    std::string state;

    std::vector<CRGB*> leds;
    void addLeds(CRGB* led);
    const char* serialize() const;

    StripsState();
};
} // namespace LedStrips
} // namespace eaf

#endif
