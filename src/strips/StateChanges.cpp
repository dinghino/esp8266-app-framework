#include "strips/StateChanges.h"

void eaf::LedStrips::StateChanges::log(Print& Printer, bool logPartial) const
{
    Printer.println("State changes");

    if (logPartial && color == true)
        Printer.printf("- %-12s %s\n", "color", color ? "true" : "false");
    if (logPartial && brightness == true)
        Printer.printf("- %-12s %s\n", "brightness", brightness ? "true" : "false");
    if (logPartial && white_value == true)
        Printer.printf("- %-12s %s\n", "white_value", white_value ? "true" : "false");
    if (logPartial && transition == true)
        Printer.printf("- %-12s %s\n", "transition", transition ? "true" : "false");
    if (logPartial && state == true)
        Printer.printf("- %-12s %s\n", "state", state ? "true" : "false");
    if (logPartial && effect == true)
        Printer.printf("- %-12s %s\n", "effect", effect ? "true" : "false");
    if (logPartial && color_temp == true)
        Printer.printf("- %-12s %s\n", "color_temp", color_temp ? "true" : "false");
}

void eaf::LedStrips::StateChanges::reset()
{
    color = false;
    brightness = false;
    white_value = false;
    transition = false;
    state = false;
    effect = false;
    color_temp = false;
}

bool eaf::LedStrips::StateChanges::hasChanges() const
{
    return(color
        || brightness
        || white_value
        || transition
        || state
        || effect
        || color_temp);
}
