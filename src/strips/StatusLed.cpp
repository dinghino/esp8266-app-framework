#include <strips/StatusLed.h>

eaf::LedStrips::StatusLed::Led::Led(uint n) : number(n) {}

eaf::LedStrips::StatusLed::StatusLed(CRGB* strip) : StatusLed(strip, 0, 1) {}
eaf::LedStrips::StatusLed::StatusLed(CRGB* strip, uint wifiLed, uint mqttLed)
        : m_pStrip(strip), wifi(wifiLed), mqtt(mqttLed) {}

void eaf::LedStrips::StatusLed::show()
{
    FastLED.clear();
    m_pStrip[wifi.number] = wifi.color;
    m_pStrip[mqtt.number] = mqtt.color;
    FastLED.setBrightness(32);
    FastLED.show();
}
void eaf::LedStrips::StatusLed::set(Led& led, CRGB color) { led.color = color; }
void eaf::LedStrips::StatusLed::set(uint led, CRGB color) { m_pStrip[led] = color; }
