#ifndef __EAF_LED_STRIPS__COLOR_H__
#define __EAF_LED_STRIPS__COLOR_H__

#include <string>
#include <vector>
#include <FastLED.h>

namespace eaf { namespace LedStrips {

struct RGB {
    union {
        struct {
            union {
                uint8_t r;
                uint8_t red;
            };
            union {
                uint8_t g;
                uint8_t green;
            };
            union {
                uint8_t b;
                uint8_t blue;
            };
        };
    };

    RGB() {}
    RGB(uint8_t r, uint8_t g, uint8_t b) : red(r), green(g), blue(b)
    {}
    uint asInt() const
    {
        return (uint)((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
    }
    static RGB fromInt(uint color)
    {
        RGB rgb;
        rgb.r = (color & 0xFF0000) >> 16;
        rgb.g = (color & 0x00FF00) >> 8;
        rgb.b = (color & 0x0000FF);
        return rgb;
    }

    void serialize(Print& Printer = Serial)
    {
        Printer.printf("RGB: { R %03d, G %03d, B %03d }\n", r, g, b);
    }
    bool operator!=(const uint comp) const
    {
        return !(asInt() == comp);
    }
    bool operator==(const RGB& comp) const
    {
        return r == comp.r && g == comp.g && b == comp.b;
    }
    bool operator!=(const RGB& comp) const
    {
        return !(*this == comp);
    }
};
// struct XY { uint x; uint y; };
// struct HS { uint h; uint s; };
union Color
{
    Color() : rgb() {}
    RGB rgb;
    // XY xy;
    // HS hs;
};


} // namespace LedStrips
} // namespace eaf

#endif
