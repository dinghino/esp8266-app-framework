
#include <FastLED.h>

#include <eaf/core/App.h>
#include <eaf/core/AppComponent.h>

#include "strips/StripsState.h"
#include "strips/StateChanges.h"
#include "strips/StatusLed.h"

#ifndef EAF_LEDS_LOG_UPDATES
    #define EAF_LEDS_LOG_UPDATES false
#endif
namespace eaf { namespace LedStrips {

class Controller : public AppComponent
{
    StripsState strips;
    StateChanges changes;

    // local state for toggling light status. might be overkill
    bool m_status;
    // time delay for brightness change for a single step
    uint8_t m_brgight_step_time = 2;
    // brightness delta for each brightness step change
    uint8_t m_brgight_step_size = 1;
    uint8_t m_rgb_step_time     = 2;

public:
    Controller();

    void addLeds(CRGB* strip);
    std::vector<CRGB*>& getLeds();

    void begin();
    void update();

    void handleNewState(char* newState);

    bool setState(const char* state)
    {
        setInternalState(state);
        if (strips.state != state)
        {
            changes.state = true;
            strips.state = state;
            return true;
        }
        return false;
    }

    bool setColor(uint color)
    {
        RGB rgb = RGB::fromInt(color);
        return setColor(rgb);
    }
    bool setColor(RGB color)
    {
        if (strips.color.rgb != color)
        {
            strips.color.rgb = color;
            changes.color = true;
            return true;
        }
        return false;
    }
    bool setBrightness(uint brightness)
    {
        if (strips.brightness != brightness)
        {
            if (brightness > 0)
                setState("ON");
            else
                setState("OFF");

            strips.brightness = brightness;
            changes.brightness = true;
            return true;
        }
        return false;
    }

    template <class Publisher>
    void publishState(Publisher& pub, const char* topic, bool retain = false)
    {
        pub.publish(topic, strips.serialize(), retain);
    }

private:

    void updateState();
    void updateBrightness();
    void updateColor();

    /**
     * Primary update function to sync actual lights state with stored state
     * using Changes and local state
     */
    void updateLights();

    /** update the color of a strip from the strips state stored color
     * If no stored color is available it will default to white
     */
    void updateColor(CRGB* strip);
    /** update the color of a strip to the given color.
     * NOTE: This is the actual setter
     */
    void updateColor(CRGB* strip, RGB color);
    /** Change the brightness of all strips connected to the device
     * NOTE: This is the actual setter
     */
    void updateBrightness(uint8_t target);

    /** Toggle a strip ON/OFF depending on previous state.
     * If the strip is OFF it will be set to a black light.
     * If is on it will be toggled back to stored color
     * (if any, else it needs an input)
     * NOTE: This is the actual setter
     */
    void toggle(CRGB* strip);
    void turnOn(CRGB* strip);
    void turnOff(CRGB* strip);

    void setInternalState(const char* newState);
    // template <typename ...Args>
    void logUpdate(const char* prop, const char* format, ...);
};

} // namespace LedStrips
} // namespace eaf

