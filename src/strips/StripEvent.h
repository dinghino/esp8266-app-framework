#ifndef __EAF__STRIPS__LED_EVENT_H__
#define __EAF__STRIPS__LED_EVENT_H__

#include <eaf/core/Event.h>
#include "strips/StripsState.h"

namespace eaf { namespace LedStrips {

typedef enum LED_EVENT { CHANGED } led_event_t;

struct StripEvent : public core::Event<led_event_t>
{

    const char* serialized;
    const StripsState state;

    StripEvent(StripsState& state) :
        Event(CHANGED),
        serialized(state.serialize()),
        state(state)
    {}
};

} // namespace LedStrips
    
} // namespace eaf


#endif
