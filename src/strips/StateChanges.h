#ifndef __EAF_LED_STRIPS__STATE_CHANGES_H__
#define __EAF_LED_STRIPS__STATE_CHANGES_H__

#include <Arduino.h>

namespace eaf { namespace LedStrips {


struct StateChanges
{
    // keeps track of what (needs to be) changed in order to trigger
    // stuff on the lights or dispatch the new state
    bool color = false;
    bool brightness = false;
    bool white_value = false;
    bool transition = false;
    bool state = false;
    bool effect = false;
    bool color_temp = false;

    void reset();
    void log(Print& Printer, bool logPartial = false) const;
    bool hasChanges() const;

};


} // namespace LedStrips
} // namespace eaf

#endif
