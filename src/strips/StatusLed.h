#ifndef __EAF_STRIP_CONTROLLER_H__
#define __EAF_STRIP_CONTROLLER_H__

#include <string>
#include <vector>

#include <FastLED.h>
#include <ArduinoJson.h>
#include <eaf/network/definitions.h>
#include <eaf/core/SimpleState.h>
#include <eaf/utils/logger.h>
#include "config.global.h"
#include "config.strip.h"

namespace eaf { namespace LedStrips
{

// Handler for network status leds
struct StatusLed {
    struct Led
    {
        uint number;
        CRGB color = CRGB::Red;
        Led(uint n);
    };

    CRGB* m_pStrip;
    StatusLed::Led wifi;
    StatusLed::Led mqtt;

    StatusLed(CRGB* strip, uint wifiLed, uint mqttLed);
    StatusLed(CRGB* strip);

    void show();
    void set(StatusLed::Led& led, CRGB color);
    void set(uint led, CRGB color);
    template<typename E>
    void handleEvent(const E evt, Led& led)
    {
        switch(evt)
        {
            case network::IDLE:
            case network::DISCONNECTED:
                set(led, CRGB::Red); break;
            case network::CONNECTING:
                set(led, CRGB::Gold); break;
            case network::CONNECTED:
                set(led, CRGB::Green); break;
            default: return;
        }
        show();
    }
};

}; // namespace LedStrips
}; // namespace eaf

#endif
