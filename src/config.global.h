#ifndef _APP_CONFIG_GLOBAL_H_
#define _APP_CONFIG_GLOBAL_H_

// configuration size for arduinojson
#define DVC_CFG_SIZE JSON_OBJECT_SIZE(10) + 600

#define DVC_HASS_CONFIG JSON_OBJECT_SIZE(1) + DVC_CFG_SIZE

// #ifdef  CONFIG_EXTRA_SPACE
//     #define CONFIG_EXTRA_SPACE CONFIG_EXTRA_SPACE + DVC_HASS_CONFIG
// #else
#define CONFIG_EXTRA_SPACE DVC_HASS_CONFIG
// #endif

// max command size from home assistant
#define CMD_STATE_SIZE 2 * JSON_OBJECT_SIZE(7) + 160
// HASS device class
#define DEVICE_CLASS "light"

// root topic for thiis device
#define TPC_ROOT "hass/" DEVICE_CLASS "/strip_2"

// Status topic for the home assistant hub
#define TPC_HASS_STATE "hass/status"
// topic to dispatch device configuuration
#define TPC_CONFIG_PUB TPC_ROOT "/config"
// birth / last will topic
#define TPC_STATUS_PUB TPC_ROOT "/status"
// JSON Schema publishing topic to update devices
#define TPC_STATE_PUB TPC_ROOT "/state"
// JSON Schema subscription topic
#define TPC_STATE_SUB TPC_ROOT "/state/set"


// old topics

#define COLOR_TOPIC_MAIN "strip/color"
#define BRIGHT_TOPIC_MAIN "strip/brightness"

#define COLOR_TOPIC_SECONDARY "strip/1/color"
#define BRIGHT_TOPIC_SECONDARY "strip/1/brightness"

#endif
