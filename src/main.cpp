/**/

#include <Arduino.h>
#include "config.global.h"
#include <eaf/eaf.h>
#include <eaf/components/Led.h>
#include <eaf/components/PushButton.h>
#include <eaf/components/Potentiometer.h>

#include <eaf/utils/mqtt_utils.h>

#include <eaf/network/Wifi.h>
// #include <WiFiUdp.h>
// #include <eaf/network/NtpClient.h>

#include <eaf/components/HomeAssistant.h>

// for brevity of example
using namespace network;
using namespace eaf;

// ============================================================================
// Functions declaration

void mqttMessageDebug(char *topic, byte *payload, unsigned int length);
void handleHassOnline(byte*, uint8_t);

// ============================================================================
// Object creation

// WiFiUDP ntpUDP;
// NTPClient ntp(ntpUDP, "europe.pool.ntp.org", 7200);
// app.addComponent(ntp)

App app("DEMO__WSTATION_1");
Wifi wifi;
// OTAController ota;
MqttModule mqtt(wifi);
HA::HomeAssistant homeAssistant;


// ============================================================================
// Main Functions

void setup()
{
    Serial.begin(9600);
    while(!Serial && !Serial.available());
    delay(500);

    Serial.println("\n\n");
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);

    // add all the various components to the app
    app.addComponent(wifi);
    app.addComponent(mqtt);
    app.addComponent(homeAssistant);
    
    mqtt.addCallback(mqttMessageDebug);
    // HomeAssistant topics - will replace old topics later on
    mqtt.addCallback(TPC_HASS_STATE, handleHassOnline);

    // Initialize app, will initialize the modules
    app.begin();
    Serial.println("================== DEMO APP ==================\n");
}

void loop()
{
    app.update();
}


// ============================================================================
// Functions definition

void handleHassOnline(byte*, uint8_t)
{
    homeAssistant.publishConfig(mqtt);
}

// Debug funcs ---------------------------------------------------------------

void mqttMessageDebug(char *topic, byte *payload, unsigned int length)
{
    Serial.printf("[ RCVD  ] %3dB - %-24s - ", length, topic);
    utils::mqtt::printPayload(payload, length);
}

/**/
