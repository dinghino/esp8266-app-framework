#include <eaf/utils/logger.h>

namespace utils
{
Logger::Logger(Print& printer) :
    m_print(&printer)
{}

void Logger::setPrinter(Print& printer)
{
    m_print = &printer;
}

void Logger::_simpleHeader(const char* text)
{
    m_print->printf("[ %-5s ] ", text);
}
void Logger::_composedHeader(const char*  text, int pin)
{
    m_print->printf("[ %-3s%2d ] ", text, pin);
}
void Logger::_printMessage(const char* text, bool nl)
{
    m_print->printf("%-65s", text);
    if (nl) m_print->println();
}
void Logger::_printStatus(bool status, bool nl)
{
    m_print->printf("%4s", status ? "OK" : "FAIL");
    if (nl) m_print->println();
}


void Logger::operator()(const char* module, const char* message, bool status)
{
    _simpleHeader(module);
    _printMessage(message);
    _printStatus(status);
}
void Logger::operator()(const char* module, const char* message)
{
    _simpleHeader(module);
    _printMessage(message, true);
}

void Logger::operator()(const char* module, int pin, const char* message, bool status)
{
    _composedHeader(module, pin);
    _printMessage(message);
    _printStatus(status);
}
void Logger::operator()(const char* module, int pin, const char* message)
{
    _composedHeader(module, pin);
    _printMessage(message, true);
}

Logger Log(Serial);

} // namespace utils
