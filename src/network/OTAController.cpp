#include <eaf/core/App.h>
#include <eaf/network/OTAConfig.h>
#include <eaf/network/OTAController.h>
#include <eaf/utils/logger.h>

OTAController::OTAController() :
    AppComponent(new OTAConfig())
{}

void OTAController::begin()
{
    m_ready = getConfig()->isUsable();

    if (!m_ready)
    {
        utils::Log("OTA", "Error configuring module.", false);
        return;
    }
    
    app().subscribe<network::WifiEvent>(&OTAController::onNetworkEvent, this);
}

bool OTAController::isReady() const
{
    return m_ready && m_started;
}

void OTAController::update()
{
    if (!isReady())
        return;

    ArduinoOTA.handle();
}

void OTAController::onNetworkEvent(const network::WifiEvent &evt)
{
    if (evt.type == network::DISCONNECTED)
    {
        utils::Log("OTA", "stopping.");
        m_started = false;
        return;
    }

    if (evt.type != network::CONNECTED)
        return;

    utils::Log("OTA", "Starting OTA Controller");

    auto config = getConfig<OTAConfig>();

    if (!config.hostname.empty())
        ArduinoOTA.setHostname(config.hostname.c_str());

    if (config.port != -1)
        ArduinoOTA.setPort(config.port);

    if (!config.password.empty())
        ArduinoOTA.setPassword(config.password.c_str());
    else
        utils::Log("OTA", "WARNING! No Auth configured", false);

    setupArduinoOTAEvents();

    ArduinoOTA.begin(config.useMDNS);

    utils::Log("OTA", "Ready to receive updates...", true);
    m_started = true;
}


void OTAController::setupArduinoOTAEvents()
{
    ArduinoOTA.onStart([]() {
        utils::Log("OTA", "Receving new firmware...");
    });
    ArduinoOTA.onEnd([]() {
        utils::Log("OTA", "Update Complete. rebooting...", true);
        ESP.reset();
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        // char message[20];
        // sprintf(message, "Progress: %u%%\r", progress / (total / 100));

        // utils::Log("OTA", message, false);
        Serial.printf("[ OTA   ] Upload @ %u%%\r", progress / (total / 100));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        char out[30];
        const char* message;
        switch (error)
        {
            case OTA_AUTH_ERROR:    message = "Auth Failed"; break;
            case OTA_BEGIN_ERROR:   message = "Begin Failed"; break;
            case OTA_CONNECT_ERROR: message = "Connect Failed"; break;
            case OTA_RECEIVE_ERROR: message = "Receive Failed"; break;
            case OTA_END_ERROR:     message = "End Failed"; break;
            default:                message = "Unkown Error";
        }
        sprintf(out, "Error [ %u ] %s", error, message);
        utils::Log("OTA", out, false);
    });
}
