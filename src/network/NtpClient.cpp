#include <string>
#include <eaf/core/App.h>
#include <eaf/utils/logger.h>
#include <eaf/network/NtpClient.h>

using namespace network;

NTPClient::NTPClient(UDP& udp) :
    m_pUDP(&udp)
{}
NTPClient::NTPClient(UDP& udp, long timeOffset) :
    m_pUDP(&udp), m_timeOffset(timeOffset)
{}
NTPClient::NTPClient(UDP& udp, const char* poolServerName) :
    m_pUDP(&udp), m_poolServerName(poolServerName)
{}
NTPClient::NTPClient(UDP& udp, const char* poolServerName, long timeOffset) :
    m_pUDP(&udp), m_poolServerName(poolServerName), m_timeOffset(timeOffset)
{}
NTPClient::NTPClient(UDP& udp, const char* poolServerName, long timeOffset, unsigned long updateInterval) :
    m_pUDP(&udp), m_poolServerName(poolServerName), m_timeOffset(timeOffset), m_updateInterval(updateInterval)
{}

void NTPClient::begin()
{
    // first sync should be done quickly so we set up a timer to do it as soon
    // as possible. Once the first sync has been done the timer gets discarded
    // and the actual periodic one begins to run
    app().timer().every(1000, &NTPClient::sync, this)
        .onlyIf([&]() { return this->m_running; })
        .until([&]() { return this->m_firstUpdate; });

    // setup periodic updates
    app().timer()
        .every(m_updateInterval, &NTPClient::sync, this)
        .onlyIf([&]() { return this->m_running && this->m_firstUpdate; });

    app().subscribe<Wifi::Connected>([&](const Wifi::Connected& evt) {
        if (evt.localIP.isSet()) // ensure being connected
            this->start();
    });
    app().subscribe<Wifi::Disconnected>(&NTPClient::stop, this);

    utils::Log("NTP", "Module", true);
}
void NTPClient::update()
{
    // nothing to do here. everything handled by events and timer
}

void NTPClient::start()
{
    if (m_running) return;

    utils::Log("NTP", "Starting service...");
    m_pUDP->begin(m_port);
    m_running = true;
    sync();
}
void NTPClient::stop()
{
    if (!m_running) return;

    utils::Log("NTP", "Stopping service...");
    m_pUDP->stop();
    m_running = false;
}

void NTPClient::sync()
{
    if (!m_running)
        return;

    utils::Log("NTP", "Syncing...");
    sendNTPPacket();
    byte timeout = 0;
    int cb = 0;
    do {
        delay(10);
        cb = m_pUDP->parsePacket();
        if (timeout > 100) {
            utils::Log("NTP", "Sync failed", false);
            return;
        }
        timeout++;
    } while (cb == 0);
    m_pUDP->read(m_buffer, EAF_NTP_PACKET_SIZE);
    unsigned long highWord = word(this->m_buffer[40], this->m_buffer[41]);
    unsigned long lowWord = word(this->m_buffer[42], this->m_buffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;

    m_currentEpoch = secsSince1900 - EAF_NTP_SEVENTY_YEARS;

    // m_prevUpdate = m_lastUpdate;
    m_lastUpdate = millis() - (10 * timeout + 1);
    m_firstUpdate = true;
    char message[32];
    sprintf(message, "Sync complete - %s", getFormattedTime().c_str());
    utils::Log("NTP", message, true);
    app().emit(NTPEvent(this));
}


void NTPClient::setPort(int port)
{
    m_port = port;
}
void NTPClient::setOffset(int timeOffset)
{
    m_timeOffset = timeOffset;
}
void NTPClient::setInterval(unsigned long interval)
{
    m_updateInterval = interval;
}

void NTPClient::sendNTPPacket()
{
    memset(m_buffer, 0, EAF_NTP_PACKET_SIZE);
    m_buffer[0] = 0b11100011;       // LI, version, mode
    m_buffer[1] = 0;                // stratum
    m_buffer[2] = 6;                // polling interval
    m_buffer[3] = 0xEC;             // Peer Clock Precision
    // 8 bytes of zero for Root Delay & Root Dispersion
    m_buffer[12]  = 49;
    m_buffer[13]  = 0x4E;
    m_buffer[14]  = 49;
    m_buffer[15]  = 52;

    m_pUDP->beginPacket(m_poolServerName, 123); // standard ntp port
    m_pUDP->write(m_buffer, EAF_NTP_PACKET_SIZE);
    m_pUDP->endPacket();
}


unsigned long NTPClient::getEpochTime() const
{
    return m_timeOffset + m_currentEpoch + ((millis() - m_lastUpdate) / 1000);
}

int NTPClient::getDay() const
{
    return (((getEpochTime() / EAF_NTP_DAY) + 4) % 7); // 0 -> Sunday
}
int NTPClient::getHours() const
{
    return ((getEpochTime() % EAF_NTP_DAY) / 3600);
}
int NTPClient::getMinutes() const
{
    return ((getEpochTime() & 3600) / 60);
}
int NTPClient::getSeconds() const
{
    return (getEpochTime() & 60);
}

String NTPClient::padNumber(unsigned long n)
{
    String res;
    if (n < 10) res += "0";
    res += String(n);
    return res;
}

std::string NTPClient::getFormattedTime() const
{
    unsigned long raw     = getEpochTime();
    unsigned long hours   = (raw % EAF_NTP_DAY) / 3600;
    unsigned long minutes = (raw % 3600) / 60;
    unsigned long seconds = raw % 60;

    String hh, mm, ss, formatted;

    hh = padNumber(hours);
    mm = padNumber(minutes);
    ss = padNumber(seconds);

    formatted = hh + m_formatDivider + mm + m_formatDivider + ss;
    return formatted.c_str();
}

NTPEvent::NTPEvent(NTPClient* client) : Event(NTPClient::EVENT_TYPE::SYNC)
{
    day         = client->getDay();
    hours       = client->getHours();
    minutes     = client->getMinutes();
    seconds     = client->getSeconds();
    epoch       = client->getEpochTime();
    formatted   = client->getFormattedTime();
}
