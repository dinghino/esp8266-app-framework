#ifndef __APP__MQTT_CONFIGURATION_H__
#define __APP__MQTT_CONFIGURATION_H__

#include <string>
#include <vector>
#include <eaf/core/IConfiguration.h>

struct MqttConfig : public IConfiguration
{

    struct Credentials {
        std::string username;
        std::string password;
        inline bool isSet() const { return !username.empty() && !password.empty(); }
        Credentials() {}
        Credentials(const char* user, const char* pass) :
            username(user), password(pass) {}
    };
    struct Connection {
        std::string address;
        int port;
        Connection(const char* addr, int port) :
            address(addr), port(port) {}
    };


    Credentials credentials;
    std::vector<Connection> connections;

    MqttConfig() : IConfiguration() {}

    virtual bool parse(JsonDocument& root) override
    {
        if (!root.containsKey("network"))
            return false;

        JsonObject network = root["network"];

        if (!network.containsKey("mqtt"))
            return false;

        JsonObject mqtt = network["mqtt"];

        if (mqtt.containsKey("credentials"))
        {
            JsonObject data = mqtt["credentials"];
            credentials = Credentials(data["username"], data["password"]);
        }
        if (mqtt.containsKey("connections"))
        {
            JsonArray conns = mqtt["connections"];
            for (auto conn: conns)
            {
                connections.push_back(Connection(conn["address"], conn["port"]));
            }
        }

        utils::Log("MQTT", "Configuration", true);
        return true;
    }

    void log(Print* Printer = &Serial) const override
    {
        
        Printer->printf("MQTT Credentials: %s - %s\n",
                credentials.username.c_str(), credentials.password.c_str());
        Printer->println("MQTT Available connections:");
        for (auto conn : connections)
            Printer->printf("- %s:%d\n", conn.address.c_str(), conn.port);
    }
};

#endif
