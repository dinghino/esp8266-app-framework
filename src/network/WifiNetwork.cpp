#include <eaf/network/WifiNetwork.h>

using namespace network;


WifiNetwork::WifiNetwork() : priority(-100), rssi(100) {}

bool WifiNetwork::empty() const
{
    return ssid.empty();
}
WifiNetwork::~WifiNetwork()
{}

void WifiNetwork::reset()
{
ssid = "\0";
priority = -100;
rssi = 100;
}

void WifiNetwork::log(Print* Printer)
{
    Printer->printf("[ %s ] %-20s - ch#%d @ %02ddB %s\n",
        bssid.c_str(),
        ssid.empty() ? "no ssid" : ssid.c_str(),
        channel, rssi,
        encrypted ? "(*)" : "   "
    );
}
