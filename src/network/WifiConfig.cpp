
#include <eaf/network/WifiConfig.h>
#include <eaf/utils/logger.h>

using namespace network;

WifiConfig::Network::Network(const char* ssid_, const char* pass_, int prior_) :
    ssid(ssid_), password(pass_), priority(prior_)
{}


WifiConfig::WifiConfig() :
    IConfiguration()
{}

bool WifiConfig::parse(JsonDocument& root)
{
    if (!root.containsKey("network"))
        return false;

    JsonObject network = root["network"];

    if (network.containsKey("mdns"))
        MDNS = network["mdns"].as<const char*>();

    if (network.containsKey("hostname"))
        hostname = network["hostname"].as<const char*>();

    // We don't have networks to configure, so stop here.
    // IConfiguration actually succeded though, so we return true
    if (!network.containsKey("wifi"))
        return true;

    const JsonArray credentials = network["wifi"];
    for (JsonVariant cred : credentials)
    {
        JsonObject data = cred.as<JsonObject>();
        m_networks.push_back(
            Network(data["ssid"], data["password"], data["priority"])
        );
    }
    utils::Log("WIFI", "Configuration", true);
    return true;
}

void WifiConfig::log(Print* Printer) const
{
    Printer->println("[ WIFI  ] -- Configuration:");
    Printer->println("            Credentials:");
    for (auto cred : m_networks)
            Printer->printf("            + SSID: %s PASSWORD: %s\n", cred.ssid.c_str(), cred.password.c_str());
    if (isMdnsSet())
        Printer->printf("            mDNS set to     '%s'\n", MDNS.c_str());
    if (isHostnameSet())
        Printer->printf("            Hostname set to '%s'\n", hostname.c_str());
}

