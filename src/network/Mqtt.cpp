#include "Arduino.h"
#include <eaf/core/App.h>
#include <eaf/network/Mqtt.h>
#include <eaf/network/definitions.h>
#include <eaf/utils/logger.h>
#include "network/MqttConfiguration.h"


using namespace network;

MqttModule::MqttModule(network::Wifi& network) :
    AppComponent()
{
    m_client = new PubSubClient(*network.getClient());
    setConfig(new MqttConfig());
}

MqttModule::~MqttModule()
{
    // delete m_client;
}

bool MqttModule::isConnected() const
{
    return m_client->connected();
}

void MqttModule::begin()
{
    m_state.set(STATE::IDLE, false);

    app().subscribe<network::WifiEvent>([this](const network::WifiEvent &evt) {
        return this->onNetworkStateChange(evt.type);
    });

    m_state.subscribe(&MqttModule::onStateChange, this);
    m_client->setCallback([this](char* topic, byte* payload, unsigned int length) {
        // TODO: Register directly with std::bind.. throws conversion error?
        this->mqttCallback(topic,payload,length);
    });

    // dispatch initial status
    utils::Log("MQTT", "Module", true);
}
const char* MqttModule::deviceName()
{
    if (m_device_name != nullptr)
        return m_device_name;
    else
        return app().getName();
}
void MqttModule::update()
{
    // check only if we are not trying to connect, since it wouldn't make sense
    if (m_state != STATE::CONNECTING && networkAvailable())
        m_state.set(this->isConnected() ? STATE::CONNECTED : STATE::DISCONNECTED);

    if (m_state == STATE::CONNECTED)
        m_client->loop();
}

void MqttModule::connect()
{
    MqttConfig conf = getConfig<MqttConfig>();

    // status().set(CONNECTING);

    utils::Log("MQTT", "Attempting connection loop...");

    // This is a very rough loop through all the available connections
    // TODO: Find a better algorithm and maybe try a few times on each network
    // before giving up.
    // static int n = 0;
    for (auto conn : conf.connections)
    {
        const char* address = conn.address.c_str();
        const int   port    = conn.port;

        m_currentConn.address = address;
        m_currentConn.port = port;

        status().set(STATE::CONNECTING);

        m_client->setServer(address, port);

        if (conf.credentials.isSet())
            m_client->connect(
                deviceName(),
                conf.credentials.username.c_str(),
                conf.credentials.password.c_str()
            );
        else
            m_client->connect(deviceName());

        if (m_client->connected())
        {
            status().set(STATE::CONNECTED);
            return;
        }
    } // end for
    status().set(STATE::CONNECTION_FAIL);
    // reset connection references and set disconnected
    m_currentConn.address = "";
    m_currentConn.port = -1;
}

// Mqtt specific members ------------------------------------------------------

void MqttModule::addTopic(const char *topic)
{
    m_topics.insert(topic);
}

void MqttModule::addCallback(const char *topic, MQTT_SIMPLE_CALLBACK callback)
{
    addTopic(topic);
    m_wrapped_callbacks.push_back(CallbackWrapper{topic, callback});
}
void MqttModule::addCallback(MQTT_CALLBACK callback)
{
    m_callbacks.push_back(callback);
}
bool MqttModule::publish(const char *topic, const char *payload, bool retained)
{
    return m_client->publish(topic, payload, retained);
}
// protected members ==========================================================

void MqttModule::onStateChange(const conn_state_t &state)
{
    char buffer[64]; // for debugging message

    switch (state) {
        case STATE::DISCONNECTED:
            utils::Log("MQTT", "Disconnected", false);
            timer().every(2500, &MqttModule::connect, this)
                .onlyIf([this]() { return this->networkAvailable(); })
                .until([this]() { return m_state == STATE::CONNECTED; })
                .run();
            break;
        case STATE::CONNECTED:
            sprintf(buffer, "Connected to %s", m_currentConn.address);
            utils::Log("MQTT", buffer, true);
            for (const char* topic : m_topics)
                m_client->subscribe(topic);
            break;
        case STATE::CONNECTION_FAIL:
            sprintf(buffer, "Connection to %s failed", m_currentConn.address);
            utils::Log("MQTT", buffer, false);
            m_state.set(STATE::IDLE);
            break;
        case STATE::IDLE:
        case STATE::CONNECTING:
        default:
            break;
    }

    app().emit(MqttEvent(state, this));
}

void MqttModule::onNetworkStateChange(const network::conn_state_t& state)
{
    delay(10);
    networkAvailable(state);

    if (state == CONNECTION_STATE::CONNECTED)
        connect();
    if (state == CONNECTION_STATE::IDLE || state == CONNECTION_STATE::DISCONNECTED)
        m_state.set(STATE::IDLE);
}

void MqttModule::mqttCallback(char *topic, byte *payload, unsigned int length)
{
    for (auto callback : m_callbacks) // call generics
        callback(topic, payload, length);

    for (CallbackWrapper cb : m_wrapped_callbacks) // call topic specific callback
        if (strcmp(topic, cb.topic) == 0)
            cb.callback(payload, length);
}

// Events

// FIXME: With the new configuration we are not receiving the actual
// address and port we are using
network::MqttEvent::MqttEvent(conn_state_t type, MqttModule *mqtt)
    : Event(type), client_name(mqtt->deviceName()),
    broker(mqtt->m_currentConn.address),
    port(mqtt->m_currentConn.port)
{
}
