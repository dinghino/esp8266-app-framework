#include <ESP8266WiFi.h>
#include <eaf/network/WifiScanner.h>
#include <eaf/network/WifiNetwork.h>

using namespace network;

WifiScanner::WifiScanner(bool runAsync) :
    m_isAsync(runAsync)
{}

void WifiScanner::start(bool runAsync)
{
    Serial.println("[ WIFI  ] Scanning Networks...");
    WiFi.disconnect();
    WiFi.mode(WIFI_STA);
    m_isRunning = true;
    reset();
    WiFi.scanNetworks(runAsync, true);
}

void WifiScanner::start()
{
    start(m_isAsync);
}
bool WifiScanner::isRunning()
{
    return m_isRunning;
}
bool WifiScanner::isAsync()
{
    return m_isAsync;
}
void WifiScanner::update()
{
    if (isRunning() && done())
    {
        Serial.printf("[ WIFI  ] Scan complete. Found %d networks\n", scanComplete());
        m_isRunning = false;
        return;
    }
}
bool WifiScanner::done()
{
    return scanComplete() >= 0;
}
int8_t WifiScanner::scanComplete()
{
    return WiFi.scanComplete();
}
void WifiScanner::reset()
{
    WiFi.scanDelete();
}
std::vector<WifiNetwork> WifiScanner::result()
{
    std::vector<WifiNetwork> found;
    for (int i = 0; i < scanComplete(); ++i)
    {
        WifiNetwork net;
        net.ssid            = WiFi.SSID(i).c_str();
        net.rssi            = WiFi.RSSI(i);
        net.channel         = WiFi.channel(i);
        net.encrypted       = WiFi.encryptionType(i) != ENC_TYPE_NONE;
        net.encryption_type = (wl_enc_type)WiFi.encryptionType(i);
        net.bssid           = WiFi.BSSIDstr().c_str();

        found.push_back(net);
    }
    // TODO: Call reset here, since we got the networks saved and
    // the data to use them is available?
    return found;
}

void WifiScanner::log(Print* Printer)
{
    int count = 1;
    Printer->println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    for (auto net : result())
    {
        Printer->printf("%02d) ", count);
        net.log(Printer);
        count++;
    }
    Printer->println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
}
