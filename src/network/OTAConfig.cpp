#include <eaf/network/OTAConfig.h>
#include <eaf/utils/logger.h>

OTAConfig::OTAConfig(): IConfiguration(), port(-1) {}

bool OTAConfig::parse(JsonDocument& root)
{
    if (!root.containsKey("network"))
        return false;
    
    auto network = root["network"];

    if (!network.containsKey("ota"))
        return false;

    auto ota = network["ota"];

    if (ota.containsKey("password"))
        password = ota["password"].as<const char*>();

    if (network.containsKey("mdns"))
        useMDNS = true;

    if (ota.containsKey("hostname"))
        hostname = ota["hostname"].as<const char*>();
    else if (network.containsKey("hostname"))
        hostname = network["hostname"].as<const char*>();

    if (ota.containsKey("port"))
        port = ota["port"];

    return true;
}

void OTAConfig::log(Print* Printer) const
{
    Printer->println("[ OTA   ] Configuration");
    Printer->printf("          Hostname: %s\n", hostname.c_str());
    Printer->printf("          Port: %d\n", port);
    Printer->printf("          Password: %s\n", password.c_str());
}
