#include <eaf/network/WifiEvent.h>
#include <eaf/network/Wifi.h>

using namespace network;

WifiEvent::WifiEvent(conn_state_t type, Wifi* wifi) :
    Event(type),
    localIP(WiFi.localIP()),
    server(wifi->m_network.ssid.c_str()),
    RSSI(wifi->RSSI()),
    signal_strenght(wifi->signalPercentage()),
    WiFiStatus(WiFi.status())
{}

