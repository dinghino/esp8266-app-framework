#include <ESP8266WiFi.h>
#include <eaf/core/App.h>
#include <eaf/core/AppComponent.h>
#include <eaf/network/Wifi.h>
#include <ESP8266mDNS.h>
#include <eaf/utils/logger.h>

using namespace network;

bool __isApLost(WiFiDisconnectReason reason);


Wifi::Wifi() :
    AppComponent(new WifiConfig()),
    m_client(new WiFiClient()),
    scanner(true)
{}

Wifi::~Wifi()
{
    delete m_client;
}


void Wifi::begin()
{
    utils::Log("WIFI", "Initializing");

    Config config = getConfig<Config>();

    resetWiFi();
    
    m_wifiHandlers.push_back(
        WiFi.onStationModeGotIP(std::bind(&Wifi::handleWiFiGotIP, this, std::placeholders::_1))
    );
    m_wifiHandlers.push_back(
        WiFi.onStationModeDisconnected(std::bind(&Wifi::handleWiFiDisconnect, this, std::placeholders::_1))
    );

    m_state.subscribe(&Wifi::onStatusChange, this);
    m_state.set(STATE::IDLE, false);

    if (config.isHostnameSet())
    {
        WiFi.hostname(config.hostname.c_str());
        utils::Log("WIFI", "Hostname set", true);
    }

    utils::Log("WIFI", "Module", true);
}

void Wifi::update()
{
    static bool firstRun = true;

    if (firstRun)
    {
        firstRun = false;

        if (selectNetwork())
        {
            utils::Log("WIFI", "Ready to connect", true);
        }
        else
        {
            utils::Log("WIFI", "No Network found", false);
            return;
        }

        app()
            .timer()
            .every(WIFI_DEFAULT_CONN_TIMEOUT * 2, &Wifi::connect, this)
            .onlyIf([&]() { return m_state == STATE::DISCONNECTED; })
            .run();

    }

    if (scanner.isRunning())
        return scanner.update();
}

void Wifi::connect()
{

    if (m_state == STATE::CONNECTED)
        return;

    // NOTE: Added check on m_network.empty()
    if (m_connectionAttempt >= WIFI_MAX_ATTEMPTS || m_network.empty())
        selectNetwork(true);
    
    m_connectionAttempt++;

    if (m_network.empty())
        return; // TODO: Log?

    m_state.set(STATE::CONNECTING);

    WiFi.mode(WIFI_STA);
    WiFi.begin(m_network.ssid.c_str(), m_network.password.c_str());

}

bool Wifi::beginSelectNetwork()
{
    if (getConfig<Config>().m_networks.empty())
    {
        utils::Log("WIFI", "No network configured. bailing.", false);
        return false;
    }

    if (!scanner.done()) scanner.start(false);
    scanner.update();

    if (scanner.scanComplete() <= 0)
    {
        utils::Log("WIFI", "No networks found. bailing.", false);
        m_network.reset();
        return false;
    }

    #if WIFI_LOG_SCAN_RESULTS
        scanner.log(&Serial);
    #endif
    return true;
}

bool Wifi::selectNetwork(bool skipCurrent)
{
    if (!beginSelectNetwork())
        return false;

    auto savedNetworks = getConfig<Config>().m_networks;

    std::vector<Network> nets = scanner.result();

    #if defined WIFI_LOG_SCAN_RESULTS && WIFI_LOG_SCAN_RESULTS
        scanner.log();
    #endif

    scanner.reset();
    
    static std::string currentSSID;

    if (skipCurrent)
    {
        // since we are skipping the currently selected network we can reset
        // the attempts here to avoid useless rescan later
        m_connectionAttempt = 0;
        currentSSID = m_network.ssid;
        m_network.reset();
    }

    for (auto saved : savedNetworks)
    {
        // skip current SSID if requested and it exists
        if (skipCurrent && saved.ssid.compare(currentSSID) == 0)
            continue;

        for (auto found : nets)
        {
            // ssid is no match
            if (found.ssid.compare(saved.ssid) != 0)
                continue;
            // TODO: Compare BSSID (MAC) too if possible for hidden networks
            // can't connect to encrypted w/o pass
            if (found.encrypted && saved.password.empty())
                continue;
            // get the network with the highest priority.
            // The first one we meet that we can connect to automatically passes
            if (m_network.priority <= 0 || saved.priority > m_network.priority)
            {
                m_network = found;
                m_network.password = saved.password;
                m_network.priority = saved.priority;
                continue;
            }
            // or if the priority is the same the one with better signal.
            // rssi is negative value in dB so lower the better
            if (m_network.priority == saved.priority && found.rssi < m_network.rssi)
            {
                m_network = found;
                m_network.password = saved.password;
                m_network.priority = saved.priority;
                continue;
            }
        }
    }

    if (m_network.empty())
        return false;

    Serial.printf("[ WIFI  ] Selected '%s'\n", m_network.ssid.c_str());
    return true;
}

// Internals

bool Wifi::startMdnsResponder()
{
    Config conf = getConfig<Config>();
    if (!conf.isMdnsSet())
        return false;

    if (MDNS.begin(conf.MDNS.c_str(), WiFi.localIP()))
        return true;
    return false;
}

void Wifi::handleWiFiGotIP(const WiFiEventStationModeGotIP& evt)
{
    if (!evt.ip.isSet())
        return;

    /* available fields are:  evt.gw (gateway) | evt.ip | evt.mask */
    m_connectionAttempt = 0;
    if (m_state.set(STATE::CONNECTED))
        startMdnsResponder();
}

void Wifi::handleWiFiDisconnect(const WiFiEventStationModeDisconnected& evt)
{
    /* available fields are:  evt.bssid | evt.reason | evt.ssid*/

    if (m_state != STATE::DISCONNECTED) {
        Serial.printf("[ WIFI  ] Disconnected from %s %s\n",
            evt.ssid.c_str(), __isApLost(evt.reason) ? "- AP is lost." : ""
        );
        app().emit(event::Disconnected(this, evt.reason));
    }

    m_state.set(STATE::DISCONNECTED);

    if (__isApLost(evt.reason))
        selectNetwork(true);
}

void Wifi::resetWiFi()
{
    #if RESET_WIFI
      WiFi.disconnect(NWF_DISABLE_WIFI);
      WiFi.begin();
      WiFi.disconnect(NWF_DISABLE_WIFI);
      WiFi.setAutoConnect(NWF_CONNECT);
      WiFi.setAutoReconnect(NWF_RECONNECT);
      WiFi.persistent(NWF_PERSISTENT);
      WiFi.setSleepMode(WIFI_NONE_SLEEP);
      WiFi.begin();
      m_state.set(STATE::IDLE);
    #endif
}

void  Wifi::onStatusChange(const conn_state_t& state)
{
    // emit a generic event every time the local state changes
    switch(state)
    {
        case STATE::DISCONNECTED:

            // TODO: Find a way to emit the Disconnected event here. it needs the reason
        case STATE::CONNECTED:
            Serial.printf("[ WIFI  ] Connected to %s as %s | local IP: %s\n",
                m_network.ssid.c_str(), WiFi.hostname().c_str(), WiFi.localIP().toString().c_str()
            );
            app().timer().once(100, TicToc::lambda([&]() {
                app().emit(Connected(this, WiFi.gatewayIP()));
            }));
            break;
        case STATE::CONNECTING:
            Serial.printf("[ WIFI  ] Attempting connection to %s...\n", m_network.ssid.c_str());
            app().emit(Connecting(this));
            break;
        case STATE::IDLE:
        default:
            break;
    }
    app().emit(Event(state, this));

}

// Utilities

bool Wifi::isConnected() const
{
    return WiFi.status() == WL_CONNECTED;
}

int32_t Wifi::RSSI() const
{
    return WiFi.RSSI();
}

int Wifi::signalPercentage() const
{
    int32_t rssi = RSSI();
    if (rssi <= -90)
        return 0;
    else if (rssi >= -50)
        return 100;
    else
        return 2 * (rssi + 100);
}

bool Wifi::shouldConnect() const
{
    return (m_state == STATE::IDLE || m_state == STATE::DISCONNECTED) && !m_network.empty();
}

// Local utilities ------------------------------------------------------------

bool __isApLost(WiFiDisconnectReason reason)
{
    return reason == WIFI_DISCONNECT_REASON_AUTH_LEAVE || reason == WIFI_DISCONNECT_REASON_NO_AP_FOUND;
}
