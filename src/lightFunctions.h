#ifndef __EAF_LIGHT_FUNCTIONS_H__
#define __EAF_LIGHT_FUNCTIONS_H__

#include <vector>
#include <FastLED.h>

void payloadToCharArr(byte*, uint8_t, char*);
uint payloadToInt(byte*, uint8_t);

void stripTest(std::vector<CRGB*> strips);
void fadeAll(std::vector<CRGB*> strips);

void setColor(std::vector<CRGB*>strips, uint color, int delay = 0);

#endif
