#include <eaf/components/HomeAssistant.h>

using namespace eaf;

bool HA::Config::parse(JsonDocument& root)
{
    if (!root.containsKey("hass"))
    {
        utils::Log("HASS", "Could not find configuration in JSON.", false);
        return false;
    }

    #if HA_CONFIG_EXTENDED_LOG
        utils::Log("HASS", "Parsing HomeAssistant device configuration...");
    #endif

    auto src = root["hass"];
    bool done;

    if (src.is<JsonArray>())
        done = parseArray(src);
    else
        done = parseObject(src);

    utils::Log("HASS", "Configuration", done);
    return done;
}

bool HA::Config::parseArray(JsonArray configs)
{
    bool retval = true;

    #if HA_CONFIG_EXTENDED_LOG
        char buff[32];
        sprintf(buff, "Found %d configurations to parse.", configs.size());
        utils::Log("HASS", buff);
    #endif

    for (JsonObject conf : configs)
    {
        bool done = parseObject(conf);
        if (retval)
            retval = done;
    }
    return retval;
}
bool HA::Config::parseObject(JsonObject config)
{

    if (!isValid(config))
        return false;

    String _topic = config["topic"].as<char*>();
    String _data;
    #if HA_CONFIG_EXTENDED_LOG
        char logBuffer[64];
        sprintf(logBuffer, "- %s", _topic.c_str());
    #endif
    serializeJson(config["data"], _data);

    Dispatchable item = Dispatchable(_topic, _data);
    m_data.push_back(item);

    #if HA_CONFIG_EXTENDED_LOG
        utils::Log("HASS", logBuffer, true);
    #endif

    return true;
}

bool HA::Config::isValid(JsonObject conf)
{
    bool done = true;
    if (!conf.containsKey("topic"))
    {
        utils::Log("HASS", "Cannot parse configuration without 'topic'.", false);
        done = false;
    }

    if (!conf.containsKey("data"))
    {
        utils::Log("HASS", "Cannot parse configuration 'data'.", false);
        done = false;
    }
    return done;
}

HA::HomeAssistant::HomeAssistant() : AppComponent(new HA::Config()) {}

void HA::HomeAssistant::begin()
{
    utils::Log("HASS", "Module", getConfig<Config>().isUsable());
}
void HA::HomeAssistant::update()
{}
