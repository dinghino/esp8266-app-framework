#include <eaf/components/PBEvents.h>

ButtonPressed::ButtonPressed(unsigned int pin, const char *name)
    : Event(PRESSED), gpio(pin), name(name)
{
}

ButtonReleased::ButtonReleased(unsigned int pin, const char *name, unsigned long pressTime)
    : Event(RELEASED), gpio(pin), name(name), duration(timestamp - pressTime)
{
}
