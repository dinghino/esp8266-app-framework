#include <eaf/core/App.h>

#include <eaf/components/Led.h>
#include <eaf/components/LedBehaviour.h>

// STATIC LED =================================================================

StaticLED::StaticLED(Led *led) : LedBehaviour(led)
{
}
StaticLED::~StaticLED()
{
}
void StaticLED::start()
{
    digitalWrite(m_led->pin(), HIGH);
}
void StaticLED::stop()
{
    digitalWrite(m_led->pin(), LOW);
}
void StaticLED::update()
{
    digitalWrite(m_led->pin(), m_led->state());
}

// BLINK LED ==================================================================

BlinkLED::BlinkLED(Led *led, unsigned int time) : LedBehaviour(led), mState(OFF)
{
    m_timer = &m_led->timer().every(time, &BlinkLED::toggle, this)
    .onlyIf([this]() { return m_led->state() == Led::ON; });
}
BlinkLED::~BlinkLED()
{
    m_led->timer().clear(m_timer);
}
void BlinkLED::start()
{
    mState = ON;
}
void BlinkLED::stop()
{
    mState = OFF;
}
void BlinkLED::update()
{
    digitalWrite(m_led->pin(), mState);
}
void BlinkLED::toggle()
{
    mState = mState == OFF ? ON : OFF;
}

// FADE LED ===================================================================

FadeLED::FadeLED(Led* led, unsigned int time_) : LedBehaviour(led), mValue(0), mStepSize(5)
{
    unsigned int steps = ((MAX_V - MIN_V) / mStepSize);
    unsigned int speed = time_ / steps;

    m_timer = &m_led->timer().every(speed, &FadeLED::fade, this)
    .onlyIf([this]() { return m_led->state() == Led::ON; });
}
FadeLED::~FadeLED()
{
    m_led->timer().clear(m_timer);
}
void FadeLED::start()
{
}
void FadeLED::stop()
{
    mDir = UP;
    mValue = MIN_V;
}
void FadeLED::update()
{
    analogWrite(m_led->pin(), mValue);
}
void FadeLED::fade()
{
    if (mValue >= MAX_V) mDir = DOWN;
    else if (mValue <= MIN_V) mDir = UP;

    mValue += (mStepSize * mDir);
}
