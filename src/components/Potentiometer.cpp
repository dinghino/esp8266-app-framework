#include <eaf/core/App.h>
#include <eaf/components/Potentiometer.h>
#include <eaf/utils/logger.h>

Potentiometer::Potentiometer(unsigned int pin, unsigned int precision)
    : m_pin(pin), m_precision(precision), m_mapped(false),
      m_map_min(0), m_map_max(1023)
{
}
Potentiometer::Potentiometer(unsigned int pin, unsigned int min_, unsigned int max_, unsigned int precision)
    : m_pin(pin), m_precision(precision), m_mapped(true),
      m_map_min(min_), m_map_max(max_)
{
}

    void Potentiometer::begin()
{
    m_value.set(analogRead(m_pin), false);
    m_value.subscribe(&Potentiometer::emitEvent, this);

    timer().every(10, &Potentiometer::read, this);

    utils::Log("POT", m_pin, "", true);
}

void Potentiometer::update()
{
    // Remove timer subscription and move read() here?
}

void Potentiometer::read()
{
    static const float EMA_a = 0.6;

    int current = m_value.get();
    int reading = analogRead(m_pin);

    int avg = (EMA_a * reading) + ((1 - EMA_a) * current);
    // try to avoid setting false readings. done by checking current +- precision
    if (avg > current - m_precision && avg < current + m_precision)
        return;

    m_value = avg;
}
void Potentiometer::emitEvent(const int &val)
{   
    int reading = m_mapped ? map(val, 0, 1023, m_map_min, m_map_max) : val;
    app().emit(PotValueChange(m_pin, val, false, reading, m_precision));
}

PotValueChange::PotValueChange(uint gpio, int reading, bool mapped, long value, int precision)
    : Event(POT_VALUE_CHANGE),
      gpio(gpio),
      reading(reading),
      is_mapped(mapped),
      value(value),
      precision(precision)
{
}
