#include <eaf/core/App.h>
#include <eaf/components/PushButton.h>
#include <eaf/utils/logger.h>

PushButton::PushButton(uint8_t pin, const char *name)
    : m_gpio(pin), m_mode(NORMAL), m_name(name), m_state(RELEASED)
{
}

PushButton::PushButton(uint8_t pin, const char* name, MODE mode)
    : PushButton(pin, name)
{
    m_mode = mode;
}

void PushButton::begin()
{
    pinMode(m_gpio, m_mode);
    m_state.subscribe(&PushButton::onStateChange, this);

    utils::Log("BTN", m_gpio, m_name, true);
}
void PushButton::update()
{
    bool pressed = digitalRead(m_gpio);
    if (m_mode == PULLUP)
    {
        pressed = !pressed;
    }
    m_state.set(pressed ? PRESSED : RELEASED);
}

void PushButton::onStateChange(const pb_state_t &state)
{
    if (state == PRESSED) {
        app().emit(ButtonPressed(m_gpio, m_name));
        m_lastPress = millis();
    } else {
        app().emit(ButtonReleased(m_gpio, m_name, m_lastPress));
    }
}
