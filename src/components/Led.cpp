#include <eaf/components/Led.h>
#include <eaf/components/LedBehaviour.h>
#include <eaf/utils/logger.h>

Led::Led(int pin, std::string name, STATE state)
    : AppComponent(), mPin(pin), mName(name), mStatus(state)
{
    mode(new StaticLED(this));
    if (state == ON) turnOn();
}

Led::Led(int pin, std::string name, MODE mode_, STATE state)
    : Led(pin, name, state)
{
    mode(mode_);
}

Led::~Led()
{
    delete behaviour;
}
void Led::begin()
{
    pinMode(mPin, OUTPUT);
    digitalWrite(mPin, mStatus);

    utils::Log("LED", mPin, mName.c_str(), true);
}

void Led::update()
{
    behaviour->update();
}

void Led::turnOn()
{
    mStatus = ON;
    behaviour->start();
};
void Led::turnOff()
{
    mStatus = OFF;
    behaviour->stop();
};

void Led::toggle()
{
    if (mStatus == OFF)
        turnOn();
    else
        turnOff();
}

void Led::mode(LedBehaviour* b)
{
    if (behaviour) delete behaviour;
    behaviour = b;
}
void Led::mode(led_mode_t mode_)
{
    switch (mode_) {
    case STATIC:
        mode(new StaticLED(this));
        break;
    case BLINK:
        mode(new BlinkLED(this, 250));
        break;
    case FADE:
        mode(new FadeLED(this, 250));
        break;
    default:
        return; // stop here without changing current mode
    }

    mCurrentMode = mode_;

}
