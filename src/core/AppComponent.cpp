#include <eaf/core/App.h>
#include <eaf/core/AppComponent.h>

AppComponent::~AppComponent()
{
}

void AppComponent::initialize(App *app)
{
    this->m_app = app;
    this->begin();
}

TicToc& AppComponent::timer() { return m_app->timer(); }
