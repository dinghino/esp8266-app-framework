#include <eaf/core/FileHandler.h>
#include <eaf/utils/logger.h>


FileHandler::FileHandler()
{
}
FileHandler& FileHandler::getInstance()
{
    static FileHandler instance;
    return instance;
}

bool FileHandler::begin()
{
    if (m_begin_done)
        return m_begin_success;

    if (SPIFFS.begin())
        m_begin_success = true;

    m_begin_done = true;

    utils::Log("FS", "Module", m_begin_success);

    return m_begin_success;
}

FileHandler::ReadFile FileHandler::open(const char* path)
{
    File file;

    String p = String(path);
     if (!p.startsWith("/"))
        p = "/" + p;
    path = p.c_str();

    if (!exists(path))
    {
        Serial.printf("[ FS    ] '%s' does not exists.\n", path);
        return ReadFile{file, 0};
    }

    Serial.printf("[ FS    ] Reading '%s'\n", path);
    file = SPIFFS.open(path, "r");

    if (!file.isFile())
        Serial.printf("[ FS    ] Could not open '%s'\n", path);
    else
        Serial.printf("[ FS    ] File read. Size %d B\n", file.size());

    return ReadFile{file, file.size()};
}

bool FileHandler::exists(const char *path) const
{
    return SPIFFS.exists(path);
}

void FileHandler::listContent() const
{
    utils::Log("FS", "Listing content");
    Dir dir = SPIFFS.openDir("/");
    while (dir.next())
    {
        String dname = dir.fileName();
        size_t dsize = dir.fileSize();
        Serial.printf ("         %12d B\t%s\n", dsize, dname.c_str());
    }
}

const char* FileHandler::normalizePath(const char* p)
{
    return normalizePath(String(p));
}
const char* FileHandler::normalizePath(String p)
{
    if (!p.startsWith("/"))
        p = "/" + p;
    return p.c_str();
}
