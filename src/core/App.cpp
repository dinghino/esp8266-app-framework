#include "Arduino.h"

#include <eaf/settings.h>

#include <eaf/core/App.h>
#include <eaf/core/AppComponent.h>
#include <eaf/core/ConfigSystem.h>
#include <eaf/core/FileHandler.h>
#include <eaf/utils/logger.h>


#if defined APP_CONFIG_FILE // && defined CONFIG_JSON_SIZE
#define _APP_USE_CONFIGURATOR
#endif


/**
 * TODO: Set systems inside a vector to manage them AFTER we are able to
 * create templated getters for systems, entities and components.
 * System should be all the managers - Events, Configuration, Timer and Files
 * if instanciated - with a common basic interface to automate the execution.
 * The network managers (i.e. wifi and mqtt for now) are between systems and
 * entities as they are definable by the user. For now they are entities (components)
 */

// static void _stop_everything() { while(true) { delay(5); }; }

App::App(const char *name) : m_name(name)
{
}

App::~App()
{}

bool App::begin()
{
    if (!Serial || !Serial.available())
    {
    Serial.begin(9600);
    while(!Serial && !Serial.available());
    delay(500);
    }
    Serial.println("\n-------------------------------------------------------------------------------\n");
    utils::Log("APP", "Setting up stuff...");

    if (!FileHandler::getInstance().begin())
    {
        utils::Log("APP", "Error on File System", false);
        return false;
    }

    #ifdef _APP_USE_CONFIGURATOR
        configurator = new ConfigSystem(APP_CONFIG_FILE);
        utils::Log("APP", "Config System", true);
    #else
        utils::Log("APP", "Config System", false);
    #endif

    if (configurator != nullptr)
        configurator->configure(m_components);

    // components initialization

    for (AppComponent* comp : m_components)
        comp->initialize(this);

    utils::Log("APP", "Setup", true);
    Serial.println("\n-------------------------------------------------------------------------------\n");
    return true;
}

void App::update()
{
    m_timer.update();
    for (AppComponent* comp : m_components)
        comp->update();
}

void App::addComponent(AppComponent& component)
{
    m_components.push_back(&component);
}

void App::addComponent(AppComponent* component)
{
    m_components.push_back(component);
}
