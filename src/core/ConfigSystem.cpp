
#include <eaf/core/ConfigSystem.h>

#include <vector>
#include <ArduinoJson.h>
#include <eaf/core/FileHandler.h>
#include <eaf/core/AppComponent.h>
#include <eaf/utils/logger.h>

ConfigSystem::ConfigSystem(const char* fname) :
    m_debug(false),
    m_filename(fname)
    {}
ConfigSystem::~ConfigSystem() {}

bool ConfigSystem::configure(std::vector<AppComponent*> &entities)
{
    FileHandler& fh = FileHandler::getInstance();

    auto src = fh.open(m_filename);
    File file = src.file;
    size_t jsonSize = src.size;

    DynamicJsonDocument document(jsonSize);
    char buffExpected[64];
    sprintf(buffExpected, "Expected JSON Size: %d B", jsonSize);
    utils::Log("CONF", buffExpected);

    DeserializationError error = deserializeJson(document, file);

    if (checkDeserializationError(error, jsonSize))
    {
    utils::Log("CONF", "Error reading file", false);
    return false;
    }

    // char buffExpected[100];
    sprintf(buffExpected, "Actual Document Size: %d B", document.memoryUsage());
    utils::Log("CONF", buffExpected);

    for (auto ent : entities)
    if (ent->isConfigurable())
        ent->getConfig()->configure(document);

    utils::Log("CONF", "Components", true);

    file.close();
    return true;
}


bool ConfigSystem::checkDeserializationError(DeserializationError error, size_t confSize)
{
    bool hasError = true;

    switch (error.code()) {
    case DeserializationError::IncompleteInput:
        utils::Log("CONF", "JSON input incomplete!");
        break;
    case DeserializationError::InvalidInput:
        utils::Log("CONF", "Invalid input!");
        break;
    case DeserializationError::NoMemory:
        utils::Log("CONF", "Not enough memory");
        char buffer[128];
        sprintf(buffer, "Configured size is %d B", confSize);
        utils::Log("CONF", buffer);
        break;
    case DeserializationError::NotSupported:
        utils::Log("CONF", "File content not supported");
        break;
    case DeserializationError::TooDeep:
        utils::Log("CONF", "JSON is too deep to parse.");
        break;
    case DeserializationError::Ok:
        utils::Log("CONF", "File Deserialization succeeded");
        hasError = false;
        break;
    }
    return hasError;
}
