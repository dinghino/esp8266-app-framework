# ESP8266 App Framework

> Event driven, non blocking, extensible framework for ESP8266

## What and why

I was bored to copy-paste code for every program on every esp-based board.

I was also bored of the huge number of variables needed to keep track of stuff in order
to make an actually usable program for anything more than a blink program.

The idea and mindset behind this project is similar to how any game engine or UI works:

* A central `App` core comopnent that encapsulates and manage everything else and keep the
lifecycle clean and usable but allowing modularity and expansibility for other kind of
components and logic.
* A set of core functionalities that can be used by any component
* A set of base interface that we can use and extend from in order to create other components.

The basic idea is to provide the core funcionality of the framework so  that any user can use this as a base for its own projects, create its own controller classes and use them - and eventually publish them so that we can expand the ecosystem.

In the end I wanted to try and build this for two main reason:

* Haven't found anything similar
* most importantly: why not?

## Usage example

Will come later on, but some examples can be found in the `docs/` (see below)

## Documentation

I'm working on a documentation website with mkdocs in order to keep things in check.
Everything should be available in `/docs` as `.md` files. To properly see it you'll need
`pyton 2.7` installed with `pygments`, `pymdown-extensions`, `mkdocs-material` and obviously `mkdocs`, then from `/docs/` call

    mkdocs serve

should pull up the webserver and allow to see the current documentation.

## Contributing

The project is in its super early stages; I'm still developing basic interactions between
components, need to fix state management and messaging, define guidelines (even for myself)
and add some other basic components other than the simplest one I've already built for actually
testing this stuff and its usability, **SO** for now I'm not sure I'll take in any contribution,
but if you are really eager to (or find something that REALLY REALLY REALLY needs it) feel free
to submit.
