#ifndef __APP_CORE_WIFI__DEFINITIONS_H__
#define __APP_CORE_WIFI__DEFINITIONS_H__

namespace network {

typedef enum CONNECTION_STATE
{
    IDLE                = -10,
    CONNECTION_FAIL     = -2,
    CONNECTION_LOST     = -1,
    DISCONNECTED        = 0,
    CONNECTING          = 1,
    CONNECTED           = 2
} conn_state_t;

} // namespace Network

#endif
