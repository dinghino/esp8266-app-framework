#ifndef __APP_OTA_CONFIG_H__
#define __APP_OTA_CONFIG_H__

#include <string>
#include <eaf/core/IConfiguration.h>

struct OTAConfig : public IConfiguration
{
    std::string password;
    std::string hostname;
    int port;
    bool useMDNS;

    OTAConfig();

    virtual bool parse(JsonDocument& root) override;
    virtual void log(Print* Printer) const override;

};

#endif
