#ifndef __NEW_WIFI_NETWORK_H__
#define __NEW_WIFI_NETWORK_H__

#include <string>
#include <ESP8266WiFi.h>

namespace network {

struct WifiNetwork
{
  std::string ssid;
  std::string password;
  int         priority;           // selection priority, defined in config
  int32_t     rssi;               // network stats, may be removed in the future
  int32_t     channel;
  std::string bssid;              // MAC address
  bool        encrypted;
  wl_enc_type encryption_type;
  
  WifiNetwork();
  ~WifiNetwork();

  void log(Print* Printer = &Serial);
  bool empty() const;
  void reset();
};

} // namespace network

#endif
