#ifndef __APP_MANAGER__MQTT_CONTROLLER_H
#define __APP_MANAGER__MQTT_CONTROLLER_H

#define MQTT_CONN_DELAY (unsigned long)(1 * 1000)
#define MQTT_UPDT_DELAY (unsigned long)(5 * 1000)


#include <string>
#include <vector>
#include <set>
#include "Client.h"
#include <PubSubClient.h>

#include <eaf/core/AppComponent.h>

#include <eaf/core/SimpleState.h>
#include <eaf/core/Event.h>

#include <eaf/network/definitions.h>

// #include "Network.h"  // TODO: Refactor to exclude MQTT from network namespace?

// Need to "redefine" the callback signature from PubSubClient since we can't
// put those in an vector directly, as the std::function is named and the compiler cries
#ifdef ESP8266
  #include <functional>
  #define MQTT_CALLBACK std::function<void(char *, uint8_t *, unsigned int)>
  #define MQTT_SIMPLE_CALLBACK std::function<void(uint8_t*, unsigned int)>
#else
  #define MQTT_CALLBACK void (*)(char *, uint8_t *, unsigned int)
  #define MQTT_SIMPLE_CALLBACK void (*)(uint8_t *, unsigned int)
#endif

/*
typedef enum MQTT_STATE
{
  IDLE            = -10,
  CONNECTION_FAIL = -2,
  CONNECTION_LOST = -1,
  DISCONNECTED    = 0,
  CONNECTING      = 1,
  CONNECTED       = 2,
} mqtt_state_t;
*/


typedef std::function<void(char *, uint8_t *, unsigned int)> mqtt_callback;

struct MqttEvent;


namespace network {

class Wifi;

class MqttModule : public AppComponent
{
  friend struct MqttEvent;

  using STATE = CONNECTION_STATE;

  // wrapper for callbacks registered with for specific topic
  struct CallbackWrapper
  {
    const char*          topic;
    MQTT_SIMPLE_CALLBACK callback;
  };

  struct UsedConnection
  {
    const char* address;
    int port;
    UsedConnection() : address(""), port(-1) {}
  } m_currentConn;

  public:
    MqttModule(network::Wifi& network);
    virtual ~MqttModule();

    void begin();
    void update();

    void connect();
    bool isConnected() const;

    inline void networkAvailable(bool v)
    {
      m_networkAvailable = v;
    }
    inline void networkAvailable(network::conn_state_t s)
    {
      m_networkAvailable = s == network::CONNECTION_STATE::CONNECTED;
    }
    inline bool networkAvailable() const
    {
      return m_networkAvailable;
    }

    inline PubSubClient& getClient() const
    {
      return *m_client;
    }

    inline void deviceName(const char* name)
    {
      m_device_name = (char*)name;
    }

    const char* deviceName();

    /**
     * @brief Register a topic that we need to listen to
     * 
     * @param topic topic 'address'
     */
    void addTopic(const char* topic);
    /**
     * @brief Register a simple callback for a specific topic
     *
     * Adds the topic to the pool of topics to subscribe (if not present already)
     * and register the callback.
     *
     * @param topic topic that interests the callback
     * @param callback function to call
     */
    void addCallback(const char *topic, MQTT_SIMPLE_CALLBACK callback);
    /**
     * @brief Register a standard PubSubClient callback
     *
     * Adds a callback the the pool of callbacks. The provided function
     * will be called whenever a topic is received and should act accordingly
     * Topics interested to the callback provided should be manually registered
     * for subscription.
     *
     * @param callback cb function
     */
    void addCallback(MQTT_CALLBACK callback);

    /**
     * @brief Publish a new message on the given topic
     */
    bool publish(const char* topic, const char* payload, bool retained = false);

    // network object status handler
    void onNetworkStateChange(const network::conn_state_t& state);

    // TODO: Remove. still used in main for testing but we'll hook to new Event System
    inline core::SimpleState<conn_state_t>& status() { return m_state; }

  protected:
    // internal status handler
    void onStateChange(const conn_state_t&);

    // internal mqtt callback for all registered callbacks
    void mqttCallback(char* topic, byte* payload, unsigned int length);

  protected:
    PubSubClient* m_client;
    core::SimpleState<conn_state_t> m_state;
  
    std::string  m_server;
    std::string  m_username;
    std::string  m_password;
    int          m_port;
    bool         m_networkAvailable;

    char* m_device_name;

    std::set<const char *>       m_topics;
    std::vector<MQTT_CALLBACK>   m_callbacks;
    std::vector<CallbackWrapper> m_wrapped_callbacks;
};

/**
 * @brief Base event for MQTT connection.
 */
struct MqttEvent : public core::Event<conn_state_t>
{
  MqttEvent(conn_state_t type, MqttModule *mqtt);

  const char *client_name;
  const char *broker;
  const int  port;
};

} // namespace Network

#endif
