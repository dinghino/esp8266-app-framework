#ifndef __NEW_WIFI_CONFIGURATION_H__
#define __NEW_WIFI_CONFIGURATION_H__

#include <string>
#include <vector>
#include <ArduinoJson.h>
#include <eaf/core/IConfiguration.h>

namespace network {

struct WifiConfig : public IConfiguration
{
protected:
    struct Network {
        std::string ssid;
        std::string password;
        const int   priority;
        Network(const char* ssid_, const char* pass_, int prior_);
    };

public:
    WifiConfig();

    std::vector<Network> m_networks;
    std::string          MDNS;
    std::string          hostname;

    inline bool          isMdnsSet() const { return !MDNS.empty(); };
    inline bool          isHostnameSet() const { return !hostname.empty(); }

    virtual bool parse(JsonDocument& root) override;
    void log(Print* Printer = &Serial) const override;
};

} // namespace config

#endif
