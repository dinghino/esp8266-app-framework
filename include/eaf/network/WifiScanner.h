#ifndef __NEW_WIFI_SCANNER_H__
#define __NEW_WIFI_SCANNER_H__

#include <Arduino.h>


namespace network
{
    struct WifiNetwork;

    struct WifiScanner
    {
        WifiScanner(bool runAsync = false);
        void start(bool runAsync);
        void start();
        int8_t scanComplete();
        void reset();
        void update();
        bool done();
        bool isRunning();
        bool isAsync();
        void log(Print* printer = &Serial);
        std::vector<WifiNetwork> result();
    private:
        bool m_isRunning;
        bool m_isAsync;
    };

} // namespace network


#endif
