#ifndef __NEW_WIFI_CONTROLLER_H__
#define __NEW_WIFI_CONTROLLER_H__

#include <vector>
#include <ESP8266WiFi.h>

#include <eaf/core/SimpleState.h>
#include <eaf/core/AppComponent.h>

#include <eaf/network/definitions.h>
#include <eaf/network/WifiScanner.h>
#include <eaf/network/WifiNetwork.h>
#include <eaf/network/WifiConfig.h>
#include <eaf/network/WifiEvent.h>

// Wifi configuration
#define WIFI_LOG_SCAN_RESULTS     false
#define WIFI_MAX_ATTEMPTS         3
#define WIFI_DEFAULT_CONN_TIMEOUT (unsigned int)(2.5 * 1000)
#define WIFI_DEEP_SCAN_COUNT      2
#define RESET_WIFI                true

#if RESET_WIFI
  #define NWF_CONNECT             false    // AutoConnect, if true loops until CONNECTED
  #define NWF_RECONNECT           false    // AutoReconnect, attempts to reconnect to previous network
  #define NWF_PERSISTENT          false    // Stores in FS latest valid credentisl
  #define NWF_DISABLE_WIFI        false    // Disables WiFi with WiFi.disconnect(DISABLE_WIFI)
#endif


/** WIFI Information
  ----------------------------------------------------------------- WiFi STATUS

  Available statuses from WiFi.status()

    0 -> WL_IDLE_STATUS
    1 -> WL_NO_SSID_AVAIL
    3 -> WL_CONNECTED
    4 -> WL_CONNECT_FAILED
    6 -> WL_DISCONNECTED
  - end WiFi STATUS

  ------------------------------------------------------------------- WiFi MODE

  Available modes for the WiFi module. our default is WIFI_STA but we'll need to
  Change it in the future depending on the situation (to WIFI_AP or WIFI_AP_STA)
  to configure the board from a web browser

    0 -> WIFI_OFF 
    1 -> WIFI_STA 
    2 -> WIFI_AP 
    3 -> WIFI_AP_STA 

  - end WiFi MODE

  ----------------------------------------------------------------- WiFi Events

  These are the default events that gets fired from ESP8266WiFi and to which
  we can subscribe to.

    WIFI_EVENT_STAMODE_CONNECTED
    WIFI_EVENT_STAMODE_DISCONNECTED
    WIFI_EVENT_STAMODE_AUTHMODE_CHANGE
    WIFI_EVENT_STAMODE_GOT_IP
    WIFI_EVENT_STAMODE_DHCP_TIMEOUT

    WIFI_EVENT_SOFTAPMODE_STACONNECTED
    WIFI_EVENT_SOFTAPMODE_STADISCONNECTED
    WIFI_EVENT_SOFTAPMODE_PROBEREQRECVED
    WIFI_EVENT_MAX
    WIFI_EVENT_ANY = WIFI_EVENT_MAX
    WIFI_EVENT_MODE_CHANGE

  - end WiFi Events

  ---------------------------------------------------------- Disconnect reasons

  - We could use these to determine our internal logic in case of disconnection
  - i.e. if we have a NO_AP_FOUND we can already skip the credentials.

    WIFI_DISCONNECT_REASON_UNSPECIFIED
    WIFI_DISCONNECT_REASON_AUTH_EXPIRE
    WIFI_DISCONNECT_REASON_AUTH_LEAVE
    WIFI_DISCONNECT_REASON_ASSOC_EXPIRE
    WIFI_DISCONNECT_REASON_ASSOC_TOOMANY
    WIFI_DISCONNECT_REASON_NOT_AUTHED
    WIFI_DISCONNECT_REASON_NOT_ASSOCED
    WIFI_DISCONNECT_REASON_ASSOC_LEAVE
    WIFI_DISCONNECT_REASON_ASSOC_NOT_AUTHED
    WIFI_DISCONNECT_REASON_DISASSOC_PWRCAP_BAD
    WIFI_DISCONNECT_REASON_DISASSOC_SUPCHAN_BAD
    WIFI_DISCONNECT_REASON_IE_INVALID
    WIFI_DISCONNECT_REASON_MIC_FAILURE
    WIFI_DISCONNECT_REASON_4WAY_HANDSHAKE_TIMEOUT
    WIFI_DISCONNECT_REASON_GROUP_KEY_UPDATE_TIMEOUT
    WIFI_DISCONNECT_REASON_IE_IN_4WAY_DIFFERS
    WIFI_DISCONNECT_REASON_GROUP_CIPHER_INVALID
    WIFI_DISCONNECT_REASON_PAIRWISE_CIPHER_INVALID
    WIFI_DISCONNECT_REASON_AKMP_INVALID
    WIFI_DISCONNECT_REASON_UNSUPP_RSN_IE_VERSION
    WIFI_DISCONNECT_REASON_INVALID_RSN_IE_CAP
    WIFI_DISCONNECT_REASON_802_1X_AUTH_FAILED
    WIFI_DISCONNECT_REASON_CIPHER_SUITE_REJECTED
    WIFI_DISCONNECT_REASON_BEACON_TIMEOUT
    WIFI_DISCONNECT_REASON_NO_AP_FOUND
    WIFI_DISCONNECT_REASON_AUTH_FAIL
    WIFI_DISCONNECT_REASON_ASSOC_FAIL
    WIFI_DISCONNECT_REASON_HANDSHAKE_TIMEOUT

  - end Disconnect reasons

  ------------------------------------------------ Event handlers subscriptions

  Available event subscription function for WiFi module. Each event dispatches
  a different event with different content, similar to our Event System.
  We could use these to hook up and automagically dispatch our internal state

    WiFiEventHandler  onStationModeConnected          -> void(const WiFiEventStationModeConnected &);
    WiFiEventHandler  onStationModeDisconnected       -> void(const WiFiEventStationModeDisconnected &);
    WiFiEventHandler  onStationModeAuthModeChanged    -> void(const WiFiEventStationModeAuthModeChanged &);
    WiFiEventHandler  onStationModeGotIP              -> void(const WiFiEventStationModeGotIP &);
    WiFiEventHandler  onStationModeDHCPTimeout        -> void(void);

    WiFiEventHandler  onSoftAPModeStationConnected    -> void(const WiFiEventSoftAPModeStationConnected &);
    WiFiEventHandler  onSoftAPModeStationDisconnected -> void(const WiFiEventSoftAPModeStationDisconnected &);

    WiFiEventHandler StationConnected = WiFi.onStationModeGotIP([](const WiFiEventStationModeGotIP& evt) {
        Serial.printf("Connected. IP: %s\n", evt.ip.toString().c_str());
    });

  - end Event Handlers

*/

namespace network {

    class Wifi : public AppComponent
    {
        friend struct WifiEvent;
    public:
        using STATE = CONNECTION_STATE;

        using Network = WifiNetwork;
        using Scanner = WifiScanner;
        using Config  = WifiConfig;

        using Event          = WifiEvent;
        using Connected      = event::Connected;
        using Disconnected   = event::Disconnected;
        using ConnectionLost = event::ConnectionLost;
        using Connecting     = event::Connecting;

    protected:
        std::vector<WiFiEventHandler>   m_wifiHandlers;
        core::SimpleState<conn_state_t> m_state;

        WiFiClient* m_client;
        Network     m_network;
        Scanner     scanner;
        int         m_connectionAttempt;

    public:


        Wifi();
        virtual ~Wifi();

        virtual void   begin() override;
        virtual void   update() override;

        inline Client* getClient() const { return (Client*)m_client; }

        bool    isConnected() const;

        int32_t RSSI() const;
        int     signalPercentage() const;

    protected:
        // connection manager methods
        void connect();
        bool selectNetwork(bool skipCurrent = false);
        void resetWiFi();

    private:
        void onStatusChange(const conn_state_t& state);
        bool shouldConnect() const;
        void handleWiFiGotIP(const WiFiEventStationModeGotIP& evt);
        void handleWiFiDisconnect(const WiFiEventStationModeDisconnected& evt);
        bool startMdnsResponder();
        // check config for configured networks, run the scanner and check
        // if we found anything to connect to.
        bool beginSelectNetwork();
    };

} // namespace Network

#endif
