#ifndef __APP_OTA_CONTROLLER_H__
#define __APP_OTA_CONTROLLER_H__

#include <eaf/core/AppComponent.h>
#include <ArduinoOTA.h>

class OTAController : public AppComponent
{
    bool m_ready;
    bool m_started;
    
public:
    OTAController();

    void begin() override;
    void update() override;
protected:
    bool isReady() const;
    void onNetworkEvent(const network::WifiEvent &evt);
    void setupArduinoOTAEvents();
};

#endif
