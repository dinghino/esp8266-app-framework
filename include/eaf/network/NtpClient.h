#ifndef __APP_NETWORK_NTP_CLIENT_H__
#define __APP_NETWORK_NTP_CLIENT_H__

#include <Udp.h>
#include <eaf/core/AppComponent.h>
#include <eaf/core/Event.h>

namespace network
{

#define EAF_NTP_SEVENTY_YEARS 2208988800UL
#define EAF_NTP_DAY           (unsigned long)86400L // seconds

#define EAF_NTP_PACKET_SIZE         48
#define EAF_NTP_DEFAULT_PORT        1337
// #define EAF_NTP_DEFAULT_INTERVAL    (unsigned long)(60 * (60 * 1000)) // 1  hour
#define EAF_NTP_DEFAULT_INTERVAL    EAF_NTP_DAY * 1000 // 1 day

#define EAF_DEBUG_NTP_CLIENT        true


class NTPClient : public AppComponent
{
public:
    enum EVENT_TYPE { SYNC } ntp_event_t;
private:
    UDP*          m_pUDP;
    byte          m_buffer[EAF_NTP_PACKET_SIZE];

    bool          m_running        = false;
    const char*   m_poolServerName = "pool.ntp.org";
    long          m_timeOffset     = 0; // offset for calculating local time
    unsigned long m_currentEpoch   = 0; // seconds from ntp service

    unsigned long m_updateInterval = EAF_NTP_DEFAULT_INTERVAL;
    int           m_port           = EAF_NTP_DEFAULT_PORT;

    unsigned long m_lastUpdate     = 0;
    bool          m_firstUpdate    = false;

    const char*   m_formatDivider  = ":";

public:

    NTPClient(UDP& udp);
    NTPClient(UDP& udp, long timeOffset);
    NTPClient(UDP& udp, const char* poolServerName);
    NTPClient(UDP& udp, const char* poolServerName, long timeOffset);
    NTPClient(UDP& udp, const char* poolServerName, long timeOffset, unsigned long updateInterval);

    virtual void  begin() override;
    virtual void  update() override;
    void          sync();

    inline bool   hasSynced() const { return m_firstUpdate; }

    void          setServer(const char* server);
    void          setPort(int port);
    void          setOffset(int timeOffset);
    void          setInterval(unsigned long interval);

    unsigned long getEpochTime()      const;

    int           getDay()            const;
    int           getHours()          const;
    int           getMinutes()        const;
    int           getSeconds()        const;

    std::string   getFormattedTime()  const;


protected:

    void start();
    void stop();

    void sendNTPPacket();

    static String padNumber(unsigned long n);

};

struct NTPEvent : public core::Event<NTPClient::EVENT_TYPE>
{
    int day;
    int hours;
    int minutes;
    int seconds;
    unsigned long epoch;
    std::string formatted;

    NTPEvent(NTPClient* client);
};

} // namespace network

#endif
