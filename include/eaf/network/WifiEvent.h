#ifndef __NEW_WIFI_EVENT_H__
#define __NEW_WIFI_EVENT_H__

#include <Arduino.h>
#include <ESP8266WiFi.h>

#include <eaf/core/Event.h>
#include <eaf/network/definitions.h>

namespace network
{
    class Wifi;
 
    struct WifiEvent : public core::Event<conn_state_t>
    {
        WifiEvent(conn_state_t state, Wifi* wifi);

        const IPAddress localIP;
        const char*     server;           // TODO: better name. :(
        const int32_t   RSSI;             // Actual RSSI value
        const int       signal_strenght;  // Calculated connection strenght
        wl_status_t     WiFiStatus;       // WiFi status when created
    };

    namespace event {

        struct Connected : public WifiEvent
        {
            IPAddress   gateway;
            IPAddress   ip;
            std::string mask;

            Connected(Wifi* wifi, IPAddress gw) :
                WifiEvent(CONNECTION_STATE::CONNECTED, wifi),
                gateway(gw)
            {}
        };

        struct Disconnected : public WifiEvent
        {
            WiFiDisconnectReason reason;

            Disconnected(Wifi* wifi, WiFiDisconnectReason reason) :
                WifiEvent(CONNECTION_STATE::DISCONNECTED, wifi),
                reason(reason)
            {}
        };

        struct ConnectionLost : public WifiEvent
        {
            WiFiDisconnectReason reason;

            ConnectionLost(Wifi* wifi, WiFiDisconnectReason reason) :
                WifiEvent(CONNECTION_STATE::CONNECTION_LOST, wifi),
                reason(reason)
            {}
        };
        struct Connecting : public WifiEvent
        {
            Connecting(Wifi* wifi) :
                WifiEvent(CONNECTION_STATE::CONNECTING, wifi)
            {}
        };


    } // namespace event

} // namespace Network

#endif
