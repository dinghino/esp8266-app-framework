/**
 * @brief Simple object state container for a single property
 *
 * This class is going to replace the _old_ `DataEmitter` and will be used to
 * contain and handle component state (i.e. button press state) so that we can
 * update it easily without further checking
 *
 * @file simple_state.h
 * @author Daniele Calcinai
 * @date 2018-09-23
 */

#ifndef __APP__CORE_SIMPLE_STATE_H__
#define __APP__CORE_SIMPLE_STATE_H__

#include <vector>
#include <functional>


namespace core
{

template <typename T>
struct SimpleState {

    using Callback = std::function<void(const T&)>;
    template <typename C>
    using MemberFn = void(C::*)(const T&);

    SimpleState() {}
    SimpleState(T initial) : m_value(initial) {}

    virtual ~SimpleState() {}

    /**
     * @brief try to set a nee state
     * Attempt to set the state to a value. If the value is the same
     * as before nothing will happen
     * 
     * @param val       value to set
     * @param publish   whether it should notify subscribers or not
     * @return true     state changed
     * @return false    state not changed
     */
    bool set(T val, bool publish = true)
    {
        if (val == m_value)
            return false;
        m_value = val;

        if (publish) this->notify();

        return true;
    }
    /**
     * @brief Current state value
     */
    inline T get()
    {
        return m_value;
    }
    /**
     * @brief Subscribe a member function to state changes
     */
    template <class C>
    void subscribe(MemberFn<C> clbk, C* inst)
    {
        subscribe(std::bind(clbk, inst, std::placeholders::_1));
    }
    /**
     * @brief Subscribe to state changes
     */
    void subscribe(Callback cb)
    {
        m_callbacks.push_back(cb);
    }

    void operator=(T v) { set(v); }
    bool operator==(const T v) const { return m_value == v; }
    bool operator!=(const T v) const { return m_value != v; }
    bool equals(const T v) const { return m_value == v; }

  protected:
    /**
     * @brief Notify subscribers
     */
    void notify()
    {
        if (m_callbacks.empty())
            return;

        for (Callback cb : m_callbacks)
            cb(m_value);
    }

    T m_value;

    std::vector<Callback> m_callbacks;
};

} // namespace core

#endif
