#ifndef __APP_MANAGER__APP_H__
#define __APP_MANAGER__APP_H__

#include <vector>
#include <TicToc.h>

#include <eaf/core/EventManager.h>
#include <eaf/core/ConfigSystem.h>
#include <eaf/network/Wifi.h>
#include <eaf/network/Mqtt.h>

class AppComponent;

class App : public core::EventManager
{
public:

  App(const char* name);
  ~App();
  bool begin();
  void update();

  inline const char* getName() const { return m_name; }

  void addComponent(AppComponent &component);
  void addComponent(AppComponent *component);

  inline void network(network::Wifi *network)
  {
    m_network = network;
    m_components.push_back(network);
  }
  inline void mqtt(network::MqttModule *mqttClient)
  {
    m_mqtt = mqttClient;
    m_components.push_back(mqttClient);
  }

  inline void network(network::Wifi &network_) { network(&network_); }
  inline void mqtt(network::MqttModule &mqttClient_) { mqtt(&mqttClient_); }


  inline network::Wifi &network() const { return *m_network; }
  inline network::MqttModule &mqtt() const { return *m_mqtt; }

  inline TicToc& timer() { return m_timer; }

protected:

  const char* m_name;

  TicToc m_timer;

  std::vector<AppComponent *> m_components;
  // TODO: Replaced definition to WifiModule.
  // As of now we have only the option of connecting through wifi and only
  // that implementation so NetworkModule is actually useless. We may want
  // to create a general interface for the WifiModule in order to be able to
  // redefine if needed
 network::Wifi       *m_network;
  // TODO: Generalize after MqttModule extraction from network... ?
  network::MqttModule       *m_mqtt;
  ConfigSystem*             configurator;

};

#endif
