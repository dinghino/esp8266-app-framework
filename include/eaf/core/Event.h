#ifndef __APP_CORE__EVENTS_EVENT_H__
#define __APP_CORE__EVENTS_EVENT_H__

#include <Arduino.h>


namespace core
{
/**
 * Base class for all derived events. Provide basic information about the event
 * like the timestamp and other stuff. Can (and maybe will) implement functionalities
 * for bubbling and other stuff
 */
template <typename T>
struct Event
{
    Event(T type) : type(type), timestamp(millis()), id(getNextID())
    {
    }
    virtual ~Event() {}

    const T type;
    const unsigned long timestamp;
    const unsigned int id;

  protected:
    static unsigned int getNextID()
    {
        static unsigned int id_ = 0;
        return id_++;
    }
};

} // namespace core

#endif
