#ifndef __APP_CORE__EVENTS_MANAGER_H__
#define __APP_CORE__EVENTS_MANAGER_H__

#include <functional>
#include <vector>
#include <map>


namespace core
{

struct EventManager
{
    // templated callback functor that will subscribe
    template <class Evt>
    using Callback = std::function<void(const Evt&)>;

    /**
     * @brief subscribe an object method as callback
     */
    template <typename Evt, class C>
    void subscribe(void(C::*clbk)(const Evt&), C* obj)
    {
        subscribe<Evt>(std::bind(clbk, obj, std::placeholders::_1));
    }

    template <typename Evt, class C>
    void subscribe(void(C::*clbk)(void), C* obj)
    {
        subscribe<Evt>(std::bind(clbk, obj));
    }

    /**
     * @brief subscribe a functor as callback
     */
    template <typename Evt>
    void subscribe(Callback<Evt> clbk)
    {
        size_t type = EventWrapper<Evt>::type();
        m_handlers[type].push_back(Handler<Evt>(clbk));
    }

    /**
     * @brief Emit a specific event object
     */
    template <typename Evt>
    void emit(const Evt& event)
    {
        size_t type = EventWrapper<Evt>::type();

        EventWrapper<Evt> eventWrapper(event);
        for (auto& handler : m_handlers[type])
            handler(eventWrapper);
    }
protected:

    /**
     * @brief Base class fo EventWrappers
     * basic interface for all wrappers
     */
    struct BaseEvent
    {
        virtual ~BaseEvent() {}
    protected:
        static size_t getNextType()
        {
            static size_t type_count = 0;
            return type_count++;
        }
    };

    /**
     * @brief Wrapper struct for Events objects
     * Takes care of defining the Event type and storing the event while
     * it's dispatched around.
     * @tparam Evt 
     */
    template <typename Evt>
    struct EventWrapper : BaseEvent
    {
        static size_t type()
        {
            static size_t t_type = BaseEvent::getNextType();
            return t_type;
        }

        EventWrapper(const Evt& event) : event_(event) {}

        const Evt& event_;
    };

    /**
     * @brief Wrapper for callback functors
     * Takes in a function/functor for a specific type of Event on construction.
     * on call (operator()) takes a generic BaseEvent and casts it to the correct type
     * @tparam Evt Event class desired. can be anything
     */
    template <typename Evt>
    struct Handler
    {
        Handler(Callback<Evt> callable) : m_clbk(callable) {}
        void operator() (const BaseEvent& event)
        {
            m_clbk(static_cast<const EventWrapper<Evt>&>(event).event_);
        }
        Callback<Evt> m_clbk;
    };

    // Map for all event handlers, mapped for each event type dispatched
    std::map<size_t, std::vector<Callback<BaseEvent>> > m_handlers;

};

} // namespace core

#endif
