#ifndef __APP_CORE__ICONFIGURATION_H__
#define __APP_CORE__ICONFIGURATION_H__

#include <ArduinoJson.h>

struct IConfiguration
{
    bool isLoaded() const { return m_isLoaded; }
    bool isUsable() const { return m_success; }

    virtual bool configure(JsonDocument& root) final
    {
        m_success = parse(root);
        m_isLoaded = true;
        return m_success;
    }

    virtual void log(Print* Printer) const
    {}

protected:
    bool m_isLoaded;
    bool m_success;
    virtual bool parse(JsonDocument& root) = 0;
};

#endif
