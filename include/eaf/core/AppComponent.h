#ifndef __APP_MANAGER__APP_COMPONENT_H__
#define __APP_MANAGER__APP_COMPONENT_H__

#include <TicToc.h>
#include <eaf/core/IConfiguration.h>

class App;

/**
 * TODO: Make CRTP to automate some stuff
 * Idea would be for begin() to become begin(App* app) and do some stuff
 * before calling the actual begin of the derived class
 */

class AppComponent
{
    public:
    
    AppComponent() {}

    AppComponent(App* app, IConfiguration* config = nullptr) :
      m_app(app), m_config(config) {}
    AppComponent(IConfiguration* config) :
      m_config(config) {}
    
    virtual ~AppComponent();

    virtual void begin() = 0;
    virtual void update() = 0;

    inline App& app() { return *m_app; }

    TicToc& timer();


    inline bool isConfigurable() { return getConfig() != nullptr; }

    inline IConfiguration* getConfig() { return m_config; }

    template <typename T>
    inline T getConfig() { return *static_cast<T*>(m_config); }

    template <typename T>
    inline void setConfig(T* config) { m_config = static_cast<IConfiguration*>(config); }
  protected:
    App* m_app;

  private:
    friend class App;
    IConfiguration* m_config;

    void initialize(App* app);
};

#endif
