#ifndef __APP_CORE__FILE_READER_H__
#define __APP_CORE__FILE_READER_H__

#include <FS.h>
#include <functional>

struct FileHandler
{
    typedef std::function<void(File&)> FileFoundCb;
    typedef std::function<void(void)> FileNotFoundCb;
    
    struct ReadFile
    {
      File file;
      size_t size;
    };
    
    static FileHandler& getInstance();
    bool begin();
    ReadFile open(const char* path);
    // bool read(const char* path, FileFoundCb = nullptr, FileNotFoundCb = nullptr);
    bool exists(const char* path) const;
    // delete operator for singleton
    FileHandler(FileHandler const &)    = delete;
    void operator=(FileHandler const &) = delete;
    void listContent() const;
  protected:
    bool m_begin_done;
    bool m_begin_success;

  private:
    FileHandler();
    const char* normalizePath(const char* p);
    const char* normalizePath(String p);
};

#endif // __APP_CORE__FILE_READER_H__
