#ifndef __APP_CORE__CONFIG_SYSTEM_H__
#define __APP_CORE__CONFIG_SYSTEM_H__

// #include <FS.h>
#include <functional>
#include <ArduinoJson.h>

#include <eaf/core/FileHandler.h>
#include <eaf/core/IConfiguration.h>

#include <eaf/core/AppComponent.h>

// template <const size_t Size>
struct ConfigSystem
{

protected:
    /** @deprecated */
    bool                 m_isLoaded;
    bool                 m_debug;
    const char*          m_filename;
public:
    ConfigSystem(const char* fname);
    ~ConfigSystem();
    bool configure(std::vector<AppComponent*> &entities);
  private:
    static bool checkDeserializationError(DeserializationError error, size_t confSize);
};

#endif // __APP_CORE__CONFIG_SYSTEM_H__
