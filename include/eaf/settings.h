#ifndef __APP_SETTINGS_H__
#define __APP_SETTINGS_H__

/**
 * defines the configuration file name, including path
 */
#ifndef APP_CONFIG_FILE
    #define APP_CONFIG_FILE "/config.json"
#endif

/**
 * expected size for the configuration data structure.
 * The macros are from ArduinoJson and the calculation can be done
 * from https://arduinojson.org/v6/assistant/ using an example structure of
 * what our data is meant to be
 *
 * @deprecated we now use the file size to determine the expected size of the
 * JSON for ArduinoJSON. this stuff remains here for the time being to be
 * sure that it works correctly but it seems to be ok
 */
#ifndef CONFIG_JSON_SIZE

    // Size calculator configuration defaults. these can be overridden by
    // defining the values before including this file, so that the custom ones
    // will be used to evaluate the final configur:ation json size

    #ifndef WIFI_CONN_COUNT
        /**
         * Number of WIFI Connections stored in the file
         * Default is 2
         */
        #define WIFI_CONN_COUNT 3
    #endif

    #ifndef WIFI_CONN_SIZE
        /**
         * Max number of fields expected for connection
         * Default is 3 -> { ssid, password, priority }
         *
         * Note that if changing this value, WIFI_CONN_EXTRA should be 
         * changed too to mirror the new data size (or data should be smaller)
         */
        #define WIFI_CONN_SIZE 3
    #endif
    #ifndef WIFI_CONN_EXTRA
        /**
         * Max size of the fields for each connection
         * Default is for 2 * char[24] + 1 * int
         */
        #define WIFI_CONN_EXTRA 90
    #endif

    #ifndef MQTT_CRED_SIZE
        /**
         * Max number of fields expected for mqtt credentials.
         * Default is 2 -> { username, password }
         *
         * Note that if changing this value, MQTT_CRED_EXTRA should be 
         * changed too to mirror the new data size (or data should be smaller)
         */
        #define MQTT_CRED_SIZE 2
    #endif
    #ifndef MQTT_CRED_EXTRA
        /** Max size of the fields for each MQTT Credential object.
         * Default is for 2 * char[24]
         */
        #define MQTT_CRED_EXTRA 80
    #endif
    #ifndef MQTT_ADDRS_COUNT
        /**
         * Number of addresses store to reach the MQTT Broker
         * Default is 2
         */
        #define MQTT_ADDRS_COUNT 2
    #endif
    #ifndef MQTT_ADDRS_SIZE
        /**
         * Number of fields for each MQTT address
         * Default is 2 { address, port }
         */
        #define MQTT_ADDRS_SIZE 2
    #endif
    #ifndef MQTT_ADDRS_EXTRA
        /**
         * Max size of each MQTT Address.
         * Default is for 2 * char[24]
         */
        #define MQTT_ADDRS_EXTRA 50
    #endif

    #ifndef CONFIG_EXTRA_SPACE
        /**
         * Extra random space that may be needed ad hoc
         * Default is 0
         */
        #define CONFIG_EXTRA_SPACE 0
    #endif


    // General parameters
        // root.deviceName -> string, char[24]
        #define __DEVICE_NAME_SIZE JSON_OBJECT_SIZE(1) + 40
        // extra space required for each "root" object or array,
        // meaning items that are just keys to collections and not values
        #define __OBJ_EXTRA_SPACE 10

        #define __OTA_CONFIGURATION JSON_OBJECT_SIZE(1) + JSON_OBJECT_SIZE(3) + 80
    // end General parameters

    // actual size calculation ================================================
    // Network
        // root fields insize the network structure
        // mdns, wifi, mqtt
        #define _NETWORK_KV_PAIRS 3

        // jjust a string, char[24]. Key is in network root as JSON_OBJECT_SIZE count
        #define _MDNS_SIZE        40
        // hostname for the device
        #define _HOSTNAME_SIZE    40

        // wifi networks array size + count * object size + count * values size
        #define _WIFI_CONNECTIONS_SIZE \
            JSON_ARRAY_SIZE(WIFI_CONN_COUNT) \
            + (WIFI_CONN_COUNT * JSON_OBJECT_SIZE(WIFI_CONN_SIZE)) \
            + (WIFI_CONN_COUNT * WIFI_CONN_EXTRA)


        // mqtt credentials -> address char[24] port int
        #define _MQTT_CREDENTIALS_SIZE JSON_OBJECT_SIZE(MQTT_CRED_SIZE) + MQTT_CRED_EXTRA
        // mqtt broker addresses size + count * object size + count * value size
        #define _MQTT_CONNECTIONS_SIZE \
            JSON_ARRAY_SIZE(MQTT_ADDRS_COUNT) \
            + (MQTT_ADDRS_COUNT * JSON_OBJECT_SIZE(MQTT_ADDRS_SIZE)) \
            + (MQTT_ADDRS_COUNT * MQTT_ADDRS_EXTRA)



        #define __WIFI_SIZE _WIFI_CONNECTIONS_SIZE
        // mqtt root + keys + actual sizes
        #define __MQTT_SIZE \
            JSON_OBJECT_SIZE(2) + _MQTT_CREDENTIALS_SIZE + _MQTT_CONNECTIONS_SIZE


        #define __NETWORK_SIZE \
            JSON_OBJECT_SIZE(_NETWORK_KV_PAIRS) \
            + __OTA_CONFIGURATION \
            + _MDNS_SIZE + _HOSTNAME_SIZE \
            + __WIFI_SIZE + __MQTT_SIZE \
            + (__OBJ_EXTRA_SPACE * _NETWORK_KV_PAIRS)
    // end Network


    #define CONFIG_JSON_SIZE \
        CONFIG_EXTRA_SPACE \
        + __DEVICE_NAME_SIZE \
        + __NETWORK_SIZE

#endif

#endif
