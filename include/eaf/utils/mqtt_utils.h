#ifndef __UTILS__MQTT_UTILS_H__
#define __UTILS__MQTT_UTILS_H__

#include <Arduino.h>
#include <eaf/utils/net_utils.h>

namespace utils { namespace mqtt {

void payloadToCharArr(byte* payload, uint8_t length, char* buffer)
{
    for (int i = 0; i < length; i++)
        buffer[i] = payload[i];
    buffer[length] = '\0';
}



// Debug functions to print stuff on Serial
void printPayload(byte* msg, unsigned int length)
{
    for (unsigned int i = 0; i < length; i++)
        Serial.print((char)msg[i]);
    Serial.println();
}

// utility to properly format and print the payload of an mqtt message
uint payloadToInt(byte *payload, uint8_t length)
{
    char buffer[length];
    payloadToCharArr(payload, length, buffer);
    unsigned int value = atoi(buffer);
    return value;
}

const char* status_to_string(network::conn_state_t state)
{
    return utils::net::status_to_string(state);
}
} // namespace mqtt
} // namespace utils

#endif
