#ifndef __UTILS__NET_UTILS_H__
#define __UTILS__NET_UTILS_H__
#include <eaf/network/definitions.h>

namespace utils { namespace net {

const char* status_to_string(network::conn_state_t state)
{
    using namespace network;
    switch (state)
    {
    case CONNECTION_LOST:
        return "CONNECTION_LOST";
    case CONNECTION_FAIL:
        return "CONNECTION_FAIL";
    case IDLE:
        return "IDLE";
    case DISCONNECTED:
        return "DISCONNECTED";
    case CONNECTING:
        return "CONNECTING";
    case CONNECTED:
        return "CONNECTED";
    default:
        return "UNKOWN STATE";
    }
}

} // namespace network
} // namespace utils

#endif
