#ifndef __APP__MODULE_LOG_H__
#define __APP__MODULE_LOG_H__

#include <Arduino.h>

namespace utils
{

class Logger {
    Print* m_print;
public:
    Logger(Print& printer);

    void setPrinter(Print& printer);

    void operator()(const char* module, const char* message, bool status);

    void operator()(const char* module, int pin, const char* message, bool status);
    void operator()(const char* module, int pin, const char* message);

    void operator()(const char* module, const char* message);

protected:
    void _simpleHeader(const char* text);
    void _composedHeader(const char*  text, int pin);
    void _printMessage(const char* message, bool nl = false);
    void _printStatus(bool status, bool nl = true);
    
};

extern Logger Log;

} // namespace utils

#endif
