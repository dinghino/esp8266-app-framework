#ifndef __APP_MANAGER__COMP_PB_EVENTS_H
#define __APP_MANAGER__COMP_PB_EVENTS_H

#include <eaf/core/Event.h>

enum pb_state_t
{
    RELEASED,
    PRESSED
};

struct ButtonPressed : public core::Event<pb_state_t>
{
    ButtonPressed(unsigned int pin, const char *name);
    const unsigned int gpio;
    const char*        name;
};

struct ButtonReleased : public core::Event<pb_state_t>
{
    ButtonReleased(unsigned int pin, const char *name, unsigned long pressTime);
    const unsigned int    gpio;
    const char*           name;
    const unsigned long   duration;
};

#endif