#ifndef __APP_MANAGER__PUSH_BUTTON_H__
#define __APP_MANAGER__PUSH_BUTTON_H__

#include <Arduino.h>
#include <functional>
#include <vector>

#include <eaf/core/AppComponent.h>
#include <eaf/core/SimpleState.h>

#include <eaf/components/PBEvents.h>

class PushButton : public AppComponent
{
  public:

    enum MODE { NORMAL = INPUT, PULLUP = INPUT_PULLUP };

    PushButton(uint8_t pin, const char* name);
    PushButton(uint8_t pin, const char* name, MODE mode);

    void begin();
    void update();
    inline core::SimpleState<pb_state_t> &status() { return m_state; }

  protected:

    void onStateChange(const pb_state_t& state);

    uint8_t       m_gpio;
    MODE     m_mode;
    const char*   m_name;
    unsigned long m_lastPress;

    core::SimpleState<pb_state_t> m_state;
};

#endif