#ifndef __DNG__APP_MANAGER_LED_BEHAVIOUR__
#define __DNG__APP_MANAGER_LED_BEHAVIOUR__

#include <Arduino.h>
#include <TicToc.h>

class Led;

class LedBehaviour
{
  public:
    LedBehaviour(Led* led) : m_led(led) {}
    virtual ~LedBehaviour() {}
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual void update() = 0;

  protected:
    Led *m_led;
};

class StaticLED : public LedBehaviour
{
  public:
    StaticLED(Led* led);
    ~StaticLED();
    void start() override;
    void stop() override;
    void update() override;
};

class BlinkLED : public LedBehaviour
{
  public:
    BlinkLED(Led* led, unsigned int time = 500);
    ~BlinkLED();
    void start() override;
    void stop() override;
    void update() override;
  protected:

    void toggle();

    Timer* m_timer;
    enum STATE { OFF = LOW, ON = HIGH } mState;
};

class FadeLED : public LedBehaviour
{
  public:
    FadeLED(Led *led, unsigned int time = 500);
    ~FadeLED();
    void start() override;
    void stop() override;
    void update() override;
  protected:

    void fade();

    Timer* m_timer;
    unsigned int              mValue;
    unsigned int              mStepSize;
    static const unsigned int MAX_V = 255;
    static const unsigned int MIN_V = 0;

    enum DIRECTION { DOWN = -1, UP = 1 } mDir;
};

#endif