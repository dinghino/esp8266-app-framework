#ifndef __APP_MANAGER__I_LED_H__
#define __APP_MANAGER__I_LED_H__

#include <Arduino.h>
#include <eaf/core/AppComponent.h>

struct LedBehaviour;

class Led : public AppComponent
{
  public:
    typedef enum STATE { OFF = LOW, ON = HIGH } led_status_t;
    typedef enum MODE { STATIC, BLINK, FADE } led_mode_t;

    Led(int pin, std::string name, STATE state = OFF);
    Led(int pin, std::string name, MODE mode, STATE state = OFF);
    virtual ~Led();

    virtual void begin();
    virtual void update();
    virtual void turnOn();
    virtual void turnOff();
    // Toggle LED state from ON to OFF
    virtual void toggle();

    inline const int pin() const { return mPin; }
    inline const STATE state() const { return mStatus; }

    void mode(MODE bName);
    inline led_mode_t mode() { return mCurrentMode; }

  protected:

    void mode(LedBehaviour *b);

    int           mPin;
    std::string   mName;
    led_status_t  mStatus; // event emitter?
    LedBehaviour* behaviour;
    led_mode_t    mCurrentMode;
};
#endif
