#ifndef __EAF_HOME_ASSISTANT_H__
#define __EAF_HOME_ASSISTANT_H__

#include <Arduino.h>
#include <functional>
#include <vector>
#include <eaf/network/Mqtt.h>
#include <eaf/utils/logger.h>
#include "config.global.h"

#include <ArduinoJson.h>

#include <eaf/core/IConfiguration.h>
#include <eaf/core/AppComponent.h>
#include <eaf/utils/logger.h>

#define HA_CONFIG_EXTENDED_LOG false

namespace eaf {  namespace HA
{

struct Config : public IConfiguration
{
    struct Dispatchable
    {
        String topic;
        String data;
        Dispatchable(char* t, char* d) : topic(t), data(d) {}
        Dispatchable(String t, String d) : topic(t.c_str()), data(d.c_str()) {}
    };

    std::vector<Dispatchable> m_data;

    Config() : IConfiguration() {}
    ~Config() {
        m_data.clear();
    }

    std::vector<Dispatchable> getData() { return m_data; }
protected:
    bool isValid(JsonObject conf);
    virtual bool parse(JsonDocument& root);
    bool parseArray(JsonArray configs);
    bool parseObject(JsonObject config);
};

class HomeAssistant : public AppComponent
{
    uint m_configSize;
public:
    HomeAssistant();

    void begin() override;
    void update() override;

    template<class Publisher>
    bool publishConfig(Publisher& publisher)
    {
        bool completed = true;

        utils::Log("HASS", "Publishing device configuration");
        auto config = getConfig<Config>();

        for (auto conf : config.getData())
        {
            const char* topic = conf.topic.c_str();
            const char* data  = conf.data.c_str();

            bool done = publisher.publish(topic, data);
            utils::Log("HASS", topic, done);
            if (!done) completed = done;
        }
        return completed;
    }
    void handleHassOnline(uint8_t* , unsigned int length);
 };

}; // namespace HA
}; //namespace eaf

#endif
