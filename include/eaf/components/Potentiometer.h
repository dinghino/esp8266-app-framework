#ifndef __APP_MANAGER__POTENTIOMETER_H__
#define __APP_MANAGER__POTENTIOMETER_H__

// #include <TicToc.h>

#include <eaf/core/AppComponent.h>

#include <eaf/core/SimpleState.h>
#include <eaf/core/Event.h>

enum pot_evt_t { POT_VALUE_CHANGE };

struct PotValueChange;

class Potentiometer : public AppComponent
{
public:
    /**
     * @brief Construct a new Potentiometer object
     * Create a rotary encoder controller.
     * Fires `PotValueChange` when the hardware is moved. The event will contain
     * a `reading` with the actual read (10bit precision) and a `value` that
     * will be the same as `reading`.
     * Use `precision` parameter to tune the margin of error to avoid false
     *
     * readings
     * @param pin       analog pin to which the encoder is connected
     * @param precision false reading margin
     */
    Potentiometer(unsigned int pin, unsigned int precision = 5);
    /**
     * @brief Construct a new Potentiometer object
     * Create a new rotary encoder controller with mapping functionality.
     * Events fired from the encoder will contain the actual `reading` and
     * the mapped `value`.
     * Use `precision` parameter to tune the margin of error to avoid false
     *
     * @param pin  analog pin to which the encoder is connected
     * @param min_ min value for the map funciton
     * @param max_ max value for the map function
     * @param precision false reading margin
     */
    Potentiometer(unsigned int pin, unsigned int min_, unsigned int max_, unsigned int precision = 5);
    void begin();
    void update();

  protected:

    friend struct PotValueChange;

    void read();
    void emitEvent(const int& val);

    int  m_pin;          // pin connected. nodemcu has only A0
    int  m_precision;    // reading accuracy. lower number means higher precision
    int  m_readings[10]; // readings for the current average

    bool m_mapped;       // true if map values have been passed on construction
    unsigned int m_map_min;
    unsigned int m_map_max;

    core::SimpleState<int> m_value; // last actual average reading
};

struct PotValueChange : public core::Event<pot_evt_t>
{
    PotValueChange(uint gpio, int reading, bool mapped, long value, int precision);
    const unsigned int gpio;
    const int          reading;
    const bool         is_mapped;
    const long         value;
    const int          precision;
};

#endif