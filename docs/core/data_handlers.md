# Data handlers

Data handlers (temporary name) are those components that can handle data through a network created with a [network handler](/core/network_handlers), either by connecting to remote source to get data or acting as publisher and sending information to clients.

Examples could be:

* Mqtt client (implemented now with PubSubClient as actual client)
* Http client for REST API, either inbound or outbound
* a web server with basic http capabilities, to serve pages (i.e. device configuration pages)
* ...

