title: Messaging system
hero: Core systems
path: src/core/events
source: EventManager.h


# Introduction

!!! info
    Documentation for the Messaging System, as well as the system itself, are still considered a work
    in progress even though they are already in use throuhout the whole framework.
    Basic examples provided in this page should be valid and a more complete documentation will follow
    as soon as the system itself is defined.


The messaging system is one of the core elements of the whole framework, as it allows us to decouple components
from each other and make them _talk_ easily and is (or at least it would like to be) _very_ loosely based on the interface of the _DOM_ Event System.

The main concept around this system is that it had to be completeley unware of the messages it delivers but
still be able to be (relatively) typesafe.

This prerequisite is needed since we can't know _all_ the events that will exist in our framework but still be able
to pass them around.

## EventManger

As of now each component that needs to emit events needs to extend the core class `EventManager`, that will provide
the methods required to `subscribe` to and `emit` events of any kind.

!!! warning
    In the future this behavior will be most probably dropped in favor of a global messages bug/queue inside `App`.
    Just be aware of this.


## Subscribing to events

Events can be listened to through an `EventManager` object's `subscribe` function that has a (for now) weird templated signature:

```c++
app.subscribe<Evt>(Callback)
```

Where `Callback` can be either a function - even lambdas - that must have a signature of `#!c++ void(*)(const Evt&)` or Where `Evt` is the actual Event type (class) that you are listening to.

!!! example "Subscribing to an event with a function"
    ```C++
    void handleMyEvent(const MyEvent& evt)
    {
        // ...
    }

    EventManager em;

    // ...

    void setup()
    {
        em.subscribe<MyEvent>(handleMyEvent);
    }
    ```

You can also subscribe `lambda` functions and class methods

??? example "Lambda and member functions"
    ```c++
    struct MyStruct
    {
        void handler(const PositionEvent& evt) {
            // ...
        }
    };

    MyStruct obj;

    em.subscribe<PositionEvent>(&MyStruct::handler, &obj);
    em.subscribe<PositionEvent>([](const PositionEvent& evt) {
        // ...
    });
    ```

!!! info
    As of now this behaviour is kinda messy on the inside as I'm unable to
    properly register the calls with `std::bind` se we use a lambda instead.
    This will be fixed as soon as possible (IF it is) and will allow you to call subscribe
    without the need to specify the template parameter.


## Creating events

Every event should extend from the core `Event` class (`src/core/events/Event.h`) that will provide basic functionality
and properties that all events should have. An example is provided below.

`Event`s are divided in _families_ of event that have a `type` in common. As of now this has been tested with `enum`s, but will test with something else.

## Generic usage
??? example "Basic example"

    ```c++
    #include <Event.h>
    #include <EventManager.h>

    enum pos_evt_t { POS_CHANGED };

    struct PositionEvent : public Event<pos_evt_t>
    {
        PositionEvent(int x, int y): Event(POS_CHANGED), x(x), y(y) {}
        const int x, y;
    };

    void onPositionChange(const PositionEvent& evt)
    {
        std::cout << "POS EVT - time: " << evt.timestamp << "  | ";
        std::cout << evt.x << ", " << evt.y << std::endl;
    }

    EventManager em;

    int main()
    {
        em.subscribe<PositionEvent>(onPositionChange);
        em.emit(PositionEvent(0,0));
        return 0;
    }
    ```


## Framework use case

Even though it's generic, this messaging system has been developed specifically for
this framework, so let's get an example usage on how it can be used as of now

??? example "Framework implementation"
    ```c++
    // PushButton.h


    struct ButtonPressed : public Event {}; // empty
    struct ButtonRelease : public Event {
        // constructor
        const unsigned long duration;
    };

    enum pb_state_t { PRESSED, RELEASED };

    class PushButton : public EventManager
    {
        // ...
        void update()
        {
            // extra logic...

            // read somewhere, for now we need to ditch the auto checker
            // DataEmitter
            if (buttonStateChanged) {
                if (m_state == PRESSED)
                    emit(ButtonReleased(millis() - lastPress));
                else
                    emit(ButtonPressed());
            }
        }
    };
    ```

    Then somewhere else, i.e. in `main.cpp`

    ```c++

    // ...
    PushButton pb; // initialize correctly

    void setup()
    {
        pb.subscribe<ButtonRelease>([](const ButtonRelease& evt) {
            Serial.printf("Button released! pressed for %lu ms\n", evt.duration);
        });
    }

    ```

    This allows us to decouple the event emission to its handlers quite easy and to emit
    any kind of data as event.


## Future refactoring

We'll probably refactor the `App` class as the unique `EventManager` that will become a _message bus_ with a `queue`
that events will be added too and pulled off and dispatched every loop, so we won't be stuck in a recursive events call if it may happen.

This will require all `AppComponent` to have a reference of `App` inside, coupling the modules to App, but since we want App to do stuff for us (manage auto event subscritions, logging and so on) this may be the only way.