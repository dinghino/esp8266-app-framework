hero: Core systems
title: Simple State
description: The simplest state manager of our framework
path: src/core/state
source: SimpleState.h

# Introduction

`SimpleState<T>` is a template class that acts as controlled proxy and to which a client ca subscribe to in order to get notified of changes.

!!! note
    A State object is currently meant to be used **internally** to components and to handle internal lifecycle.
    Common usage for now it's to hook it internally in order to emit actual events.
    
    There are a couple of cases where this may not have been yet done and we still use the state of some other component (specifically MQTT that uses Wifi state directly), but this will change soon.

The value passed to SimpleState can be anything such as a native type or an enum of any kind or even  a complex state object if it can be compared with another of its kind.

!!! WARNING
    The provided value type **MUST** implement the equality operator (`==`)

## API

The API are simple:

* `#!c++ bool(set T val, bool publish)`

    Allows to update the value, if the value is the same as before nothing will happen

    parameter | description                                             | default
    ----------| ------------------------------------------------------- | --------
    val       | value to be set, same `T`ype provided upon construction |   -
    publish   | whether we should notify subscribers                    | `true`

    Returns `true` if the value changed, `false` if it didn't

    !!! note
        The `publish` parameter set to `false` is useful when we are `begin`ning a component with an initial state that was not preset in the class definition.
        This allows us to avoid emitting the "new" initial value

* `#!c++ T get()`

    Returns the current stored value


* `#!c++ void subscribe(Callback cb)`

    Registers a callback that takes in a single argument of type `T` that will get notified when the stored value changes (if not overridden on `set`)

* `#!c++ void subscribe(MemberFn, obj)`

    Register a member funcion as callback, like `#!c++ state.subscribe(&Cls::method, this);`. Behavior is the same as the other `subscribe`


## Example usage

Using a `SimpleState` object is fairly easy but can be done for multiple things

??? example "Basic example"

    ```c++
    enum data_t { TYPE1, TYPE2 };


    struct ClientCls {
        onStateChange(const int& state) {
            // ...
        }
    };

    SimpleState<data_t> state1;
    SimpleState<int>    state2;

    ClientCls my_client;

    state1.subscribe([](const data_t& state) {
        // ...
    });
    state2.subscribe(&ClientCls::onStateChange, &my_client);

    state1.set(TYPE1);      // will call the lambda
    state2.set(1);          // will call my_client.onStateChange
    state2.set(2, false);   // won't call anybody but still update the value

    state2 = 5;             // this works too, but can't block publishing

    ```
