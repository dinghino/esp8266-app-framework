title: App
hero: Core systems
path: src/
source: App.h

An `App` object is the core element of the framework. Consider it the _game engine_ of the program, for those used to game programming.

The role of App is to contain all other components, and act as a bridge between them, managing most of the core functionalities and links between components and your actual program.



## Creating an App

For now creating an `App` object is as simple as

```c++
App app
```

## Using App

Using app is a 3 step process

* First we need to add some components to it using the correct methods in `setup`
    the default method to add a component is `addComponent(AppComponent)`, but for now there are some core components that have their own setters, such as `network()` and `mqtt()` (see [network handlers](/core/network_handlers) and [data handlers](/core/data_handlers))

    !!! TODO
        API is still in progress, so we'll need to document this better when App is better defined

* call `#!c++ app.begin()` in our setup to finalize the creation and all the components initialization
* call `#!c++ app.update()` in our main `loop` function

??? example "Basic usage"
    ```c++
    #include "App.h"
    #include "components/button/PushButton.h"

    App app;
    PushButton pb(D2, "test_button");   // random component

    void setup()
    {
        app.addComponent(pb);

        app.begin();
    }

    void loop()
    {
        app.update();
    }
    ```

## App as EventManager

The App object, beside other things is the one in charge of handling all events that the components can emit, so that you have one simple basic interface to use throughout the whole application without the need to know all the created components and their specific instances.

Events are (should) automatically known in your `main.cpp` file when you import a component for usage, so if you create a [Push Button](/components/pushbutton/pb_basics) all its events are included as well, so you just need to subscribe

!!! example "App subscription"
    ```c++
    #include "App.h"
    #include "components/button/PushButton.h"

    App app;
    PushButton pb(4, "btn 1");

    void setup()
    {
        Serial.begin(9600);
        while (!Serial);

        // not actually required to subscribe, but we won't receive anything
        // since the button won't "work"
        app.addComponent(pb);

        app.subscribe<ButtonPressed>([](const ButtonPressed& evt) {
            Serial.println("Button pressed");
        });

        app.begin();
    }

    void loop()
    {
        app.update();
    }
    ```

For a more detailed documentation please refer to the [Messaging System](/core/event_system) documentation.
