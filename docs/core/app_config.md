hero: Configure your app with SPIFFS
title: App config
description: App configuration with files
path: src/core/Config
source: Config.h

# Config file

!!! todo
    * Define exactly how the config file is structured
    * Add `config.example.json` to start from
    * Define how the various properties are used and matched (i.e. that `deviceName` will be used for all names if they are missing)
    * Make some examples
    * Specify that the user _may_ use the file to store extra program configuration data and access it from `App`