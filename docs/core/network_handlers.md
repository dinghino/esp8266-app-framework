# Network handler

A core component that manages an connection that we can use to control the device or send signals from it.
A network handler is required for a [Data handler](/core/data_handlers) to work.

* Wifi client to connect to an existing network
* Wifi server to act as access point and provide a mean to connect to the device
* Ethernet client/server to handle ethernet connections
* GSM client to handle connections through a GSM network
* ...