title: PushButton
hero: A simple press can change everything
path: src/components/button/
source: PushButton.h

# Push Button

One of the simples form of input available, a `PushButton` object is meant to handle correctly the dedicated hardware.

## Creation

Constructor signature is

`#!c++ PushButton(uint8_t pin, const char* name);`

Where:

* ==pin== is the actual _digital pin_ the button is connected to for reading
* ==name== is whatever you like, to help tell various buttons apart in events and stuff

!!! danger "API Changes"
    the `name` parameter will be probably left out in the near future

The creation of the component is a one-liner

```c++
PushButton pb(D4, "pb_1");
```

The only software interaction available is to subscribe to the [events](#Events) and react
to them accordingly (see [events documentation](/core/event_system) for more).


## Events

Every `PushButton` object emits events when **pushed** or **released**.

The event `type` can be either `PRESSED` or `RELEASED`

### ButtonPressed

Emitted when the button is initially pressed

type          | property     | note
------------- | ------------ | ----
pb_evt_t      |  `type`      | `PRESSED`
unsigned int  |  `id`        |     
unsigned long |  `timestamp` |     
unsigned int  |  `gpio`      |     
char*         |  `name`      |     


### ButtonReleased

Emitted when the button is released

type          | property     | note
------------- | ------------ | ----
pb_evt_t      |  `type`      | `RELEASED`
unsigned int  |  `id`        |     
unsigned long |  `timestamp` |     
unsigned int  |  `gpio`      |     
char*         |  `name`      |     
unsigned long |  `duration`  | delta from press to now



## Example usage

A PushButton is really a simple component but quite useful.

??? example "Simple example" 

    Simplest example of a PushButton integrated with our main `App` component

    ```c++
    #include <Arduino.h>
    #include "App.h"
    #include "components/button/PushButton.h"

    App app;
    PushButton pb(D4, "test_button");

    void setup()
    {
        Serial.begin(9600);
        app.addComponent(pb);

        pb.subscribe<ButtonPressed>([](const ButtonPressed& evt) {
            Serial.printf("Button '%s' on gpio %d pressed at %lu ms\n",
                        evt.name, evt.gpio, evt.timestamp);
        });
        pb.subscribe<ButtonReleased>([](const ButtonReleased& evt) {
            Serial.printf("Button '%s' on gpio %d released after %lu ms\n",
                        evt.name, evt.gpio, evt.duration);
        });

        app.begin();
    }

    void loop()
    {
        app.update();
    }

    ```

Whenever the button is pressed and released this should print out something like

    Button 'test_button' on gpio 2 pressed at 12874 ms

    Button 'test_button' on gpio 2 released after 128 ms


## Future versions

In the future I would want to add the ability to handle `while-press` event, in order
to trigger actions when the button is pressed and being pressed for some time.
Example usage would be

> When the button is pressed for two seconds turn off the screen.