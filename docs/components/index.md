title: Introduction to components
hero: Talk and listen to your device

# Components

Components are the essential part of the framework. Each component is built around a specific
hardware and takes care of controlling it either as `input` or `output` component - in some cases both - providing an easy way to handle it on your actual program.

The core feature of each component is that it (should) encapsulate most if not all the functions needed to handle the dedicated hardware and provide a decoupled way to get and send it information.

This communication between components is possible with the framework's [event system](/core/event_system) and properly defined setters where needed, so that we can easily hook up, for example, an output like an LED to a _input source_, like a push button with a few lines of code.


!!! note
    remember that all of this is still in an early stage of development, so components, structure and API will most likely change for most of them.


# Type of components

At the moment there is no actual discrimination between components, but conceptually there
are at least two basic types:

* [Core](#Core)
* [Input](#Input)
* [Output](#Output)
* [Hybrids](#Hybrid)


## Core
After those there is a special kind, that we consider `core` components that are required for
basic functionalities and have a special place in the framework, such as

* Network handlers, which provide a means to connect to (or create) a network
* Data handlers, that can provide a means to access or send data to and from the device, like
  mqtt and http clients, [spiffs](http://esp8266.github.io/Arduino/versions/2.0.0/doc/filesystem.html) and so on

## Input
Those that handle a piece of hardware used to provide some input to the device, such as
buttons, encoders, sensors, resistors of some kind, ...

## Output
That you can use to allow the device to give the user a feedback on what's happening, like
LEDs, displays, acustic devices, ...

## Hybrid
For some hardware is hard to define the correct type as they can act both as input and output,
for example a touch screen, but since they are not implemented yet we won't discuss them.

In the following chapter we'll explore some of the available components, their API and some example on how to use
