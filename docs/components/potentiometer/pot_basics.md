title: Potentiometer
hero: Tune your signals
path: src/components/potentiometer
source: Potentiometer.h

# Potentiometer

Controller for a generic potentiometer. Requires the _output_ pin to be connected as analog input to your board.


## Creation

Simplest form of potentiometer

`#!c++ Potentiometer(unsigned int pin, unsigned int precision);`

Advanced usage

`#!c++ Potentiometer(unsigned int pin, unsigned int min_, unsigned int max_, unsigned int precision);`

Where:

* `pin` is the actual **analog** pin the potentiometer is attached to for reading
* `precision` handles false readings. higher value means lower precision but less false reading (`default = 5`).

    This is a dynamic value since it all depends on how your project is built and is meant to work.

Creating a potentiometer is a one-line

```c++
Potentiometer pot(A0);
// Potentiometer pot(A0, 10);  // reduced precision but higher tollerance
Potentiometer pot2(A1, 0, 255); // automapper potentiometer
```


## Events

A Potentiometer emits a single event, `PotValueChange`, emitted when the reading of the potentiometer changes enough from the _current_ value to go over the set `precision`.

| type          | property     | note                        |
| ------------- | ------------ | ----                        |
| pot_evt_t     |  `type`      | enum                        |
| unsigned int  |  `id`        |                             |
| unsigned long |  `timestamp` |                             |
| unsigned int  |  `gpio`      |                             |
| unsigned int  |  `precision` | potentiometer set precision |
| int           |  `reading`   | actual _10bit_ reading (0 - 1023) |
| int           |  `value`     | reading value. if mapped contains the desired value, if not is equal to `reading` |
| bool          |  `is_mapped` | whether the `value` property is already mapped to some min/max |


## Examples

The simplest usage is to create the object, add it to `App` and listen to its events.
If needed (or desired) you can try to adjust the optional `precision` parameter to your needs and desires.

??? example "Simple potentiometer"

    ```c++
    #include <Arduino.h>
    #include "App.h"
    #include "components/potentiometer/Potentiometer.h"

    App app;
    Potentiometer pot(A0);

    void setup()
    {
        Serial.begin(9600);
        app.addComponent(pot);

        pot.subscribe<PotValueChange>([](const PotValueChange& evt) {
            Serial.printf("Potentiometer on gpio %d pressed at %lu ms\n", evt.gpio, evt.timestamp);

            int value = map(evt.reading, 0, 255, 0, 1023); // map the reading to an usable value
            Serial.printf("Current reading at %d, value is %d\n", evt.reading, evt.value);
        });

        app.begin();
    }

    void loop()
    {
        app.update();
    }

    ```

When the Potentiometer is created passing the `min_` and `max_` parameter it will automatically set the `value`
property of the emitted event to the mapped value, so it's easier to use.

??? example "Automapper potentiometer"


    ```c++
    #include <Arduino.h>
    #include "App.h"
    #include "components/potentiometer/Potentiometer.h"

    App app;
    Potentiometer pot(A1, 0, 255);
    void setup()
    {
        Serial.begin(9600);
        app.addComponent(pot);

        pot.subscribe<PotValueChange>([](const PotValueChange& evt) {
            Serial.printf("Potentiometer on gpio %d pressed at %lu ms\n", evt.gpio, evt.timestamp);
            Serial.printf("Current reading at %d, value is %d\n", evt.reading, evt.value);
        });

        app.begin();
    }

    void loop()
    {
        app.update();
    }

    ```

