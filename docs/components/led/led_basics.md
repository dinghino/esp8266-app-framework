title: Led
hero: The basic one
path: src/components/led/
source: Led.h

# Introduction

A Led object is meant to handle a single monochrome LED attached to a board pin.

Since a LED is a pure output component it does **not** emit events of any kind.


Constructor signature is

`#!c++ Led(int pin, std::string name, bool isOn);`

WHere:

* `pin` - the board digital pin (preferably PWM capable)
* `name` - an assigned name for the LED
` `isOn` - initial state of the LED (_optional_)

!!! danger "API Changes"
    the `name` parameter will be probably left out in the near future

## Basic API

A LED can be turned on and off with the appropriate methods `turnOn()` and `turnOff()` or toggled with `toggle()`.

??? example "Basic usage"
    ```c++
    #include "Arduino.h"
    #include "components/led/Led.h"

    Led led(3, "test_led");

    void setup()
    {
        led.turnOn();
    }

    void loop()
    {
        led.update();
    }
    ```

??? example "Inside App"
    ```c++
    #include "Arduino.h"
    #include "App.h"
    #include "components/led/Led.h"
    #include "components/button/PushButton.h"

    App app;
    Led led(3, "test_led");
    PushButton pb(2, "test_button");

    void setup()
    {
        app.addComponent(pb);
        app.addComponent(led);

        // subscribe to the push button press event and toggle the LED
        pb.subscribe<ButtonPressed>([&](const ButtonPressed& evt) {
            led.toggle();
        });

        app.begin();
    }

    void loop()
    {
        app.update();
    }
    ```
