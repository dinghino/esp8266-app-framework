title: Led Behaviours
hero: Customize your LED
path: src/components/led/
source: LedBehaviour.h

## Behaviors

Led can have different behavior - or `mode` - that can be used to display patterns and represent different device's statuses.

All behavior must implement (extend) from the base `LedBehaviour` class, found in `src/components/led/LedBehavior.h`.

### Changing LED behavior

To change how an LED behaves or to get the current `LedBehavior` the `Led offers a `mode()` method. When passed an `Led::MODE` it will set the behavior; if called empty will return the current one to operate on.

### Default behaviors

Some basic behaviors are alread implemented:

#### `StaticLED`

The simples and default behavior for an LED is to stay on or off undefinitely. When a LED is created this is its default 

#### `BlinkLED`

Another simple mode for a LED is to blink on and off on an interval when the LED is on.

!!! note
    As of now there is no way to change the timing of the blinking

#### `FadeLED`

A more complex mode where the LED will fade in and out from maximum to minimum brightness when turned on.

??? example "LedBehavior"
    ```c++
    #include "Arduino.h"
    #include "App.h"
    #include "components/led/Led.h"
    #include "components/button/PushButton.h"

    App app;
    Led led(3, "test_led");
    PushButton pb(2, "test_button");

    void setup()
    {
        app.addComponent(pb);
        app.addComponent(led);

        // subscribe to the push button event and switch the LED to fading
        pb.subscribe<ButtonPressed>([&](const ButtonPressed& evt) {
            led.mode(Led::FADE);
        });

        app.begin();
    }

    void loop()
    {
        app.update();
    }
    ```


### Creating your own

To create a new behavior there are 3 things to do:

1. create your behavior, extending from `LedBehaviour`
2. define its _name_ in the `MODE` enum inside the `Led` class
3. add it to the switcher inside `Led::mode(MODE)` so that it can be properly used.

#### API

Constructor signature for the base class is

`#!c++ LedBehaviour(Led* led)`

A `LedBehaviour` constructor requires to know its parent `Led` in order to operate correctly and its lifecycle consist of 3 substantial moments:

* `start()` - when the `Led` is turned ==ON==
* `stop()` - when the `Led` gets turned ==OFF==
* `update()` - on the Led update function.

The basic idea is that you handle all your business logic inside the `update()` function, such as timing, updating the current output and so on and use `start` and `stop` to initialize and reset internals.

You can check out how the included behaviours work to get an idea on how to implement yours.
